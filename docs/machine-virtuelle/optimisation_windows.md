Optimisation Windows
====================

Suppression animations, ombres
------------------------------

Depuis Windows:

1. Appuyer le combo de touches **windows** + **s**.
2. Dans la barre de recherches taper **perfomances**.
3. Sélectionner l'application ** Régler l'apparance et les performances de Windows**.

![Windows application performance](img/windows_application_regler_performance.png)

Dans l'onglet **Effets visuels**:

1. Sélectionner l'option **Ajuster afin d'obtenir les meilleures performances**.
2. **Appliquer** les changements.
3. L'étape précédente peut demander un redémarrage du système pour être appliquée.

![Windows optimisation](img/windows_performance_optimisation.png)


Services Windows
----------------

Depuis Windows:

1. Appuyer le combo de touches **windows** + **s**.
2. Dans la barre de recherches taper **msconfig**.
3. Sélectionner l'onglet **Services**.
4. **Désactiver** chacun des **services** qui n'est pas indispensable au bon fonctionnement de l'ordinateur.
5. **Appliquer** les changements.
6. L'étape précédente peut demander un redémarrage du système pour être appliquée.

![Windows service](img/windows_service.png)


Démarrage sélectif
------------------

Lorsque des services windows et/ou autres de l'ordinateur ont été désactivé, on peut voir dans l'onglet **Général** que la **Sélection du mode du démarrage**  n'est plus comme par défaut sur **Démarrage normal** mais désormais sur **Démarrage sélectif**.

![Windows démarrage sélectif](img/windows_demarrage_selectif.png)

Alimentation
------------

Pour un ordinateur portable:

1. Dans la barre d'outils au niveau du **Système tray** faire un clique droit sur l'icône de la batterie.
2. Sélectionner **Options d'alimentation**.
3. S'assurer que le mode choisi est sur **Performances élevées**.

![Windows performance batterie élévée](img/windows_performance_batterie_elevee.png)


Surveillance
------------

Dans windows:

1. Appuyer le combo de touches **Ctrl**+**Alt**+**Suppr**.
2. Sélectionner **Gestionnaire des tâches**.
3. Puis onglet **Performances**.

![Windows surveillance](img/windows_performance_surveillance.png)
