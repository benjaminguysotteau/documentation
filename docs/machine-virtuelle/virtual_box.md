VirtualBox
==========

Configuration
-------------

Dans le système d'exploitation parent **windows**.

Sélectionner l'application virtualbox puis **configuration** dans la barre d'outils.

![VirtualBox configuration](img/virtualbox_configuration.png)

### Processeur

Menu **Système**, onglet **Processeur**.

S'assurer que le nombre de coeurs (physique) du processeur ainsi que les processeurs (virtuels) soient correctement alloués (déplacer le curseur **Nombre de processeurs:** dans la zone verte) au plus proche de la zone rouge pour un maximum de performance.

![VirtualBox processeur](img/virtualbox_processeur.png)

### Mémoire vive

Menu **Système**, onglet **Carte mère**.

S'assurer que la quantité de mémoire vive soit correctement allouée (déplacer le curseur **Mémoire vive:** dans la zone verte) au plus proche de la zone rouge pour un maximum de performance

![VirtualBox mémoire vive](img/virtualbox_memoire_vive.png)

### Réseau

Menu **Réseau**, onglet **Carte 1**.

Une même machine virtuelle peut être éxécutée sur plusieurs postes en même temps, ce qui pose problème lors de la connection à tous terminaux externes (**broken pipe**, **boucle sur routage**, **...**).

Il faut générér une nouvelle **Adresse MAC** via le menu réseau et cliquant sur l'icône circulaire et en appliquant les changements.

![VirtualBox réseau](img/virtualbox_reseau.png)

### USB

Onglet **Périphériques** puis **USB** et **paramètres USB...**

![VirtualBox USB paramêtres](img/virtualbox_usb_parametres.png)

S'assurer que le controlleur USB soit bien activé.

![VirtualBox USB activer contrôlleur](img/virtualbox_usb_activer_controlleur.png)

### Dossier partagé

Menu **Dossiers partagés**.

![VirtualBox dossier partagé](img/virtualbox_dossier_partage.png)

Icône **Éditer le dossier**.

Renseigner (comme exemple ci-dessous):

1. Le **chemin absolu** sous windows.
2. Le **nom** du dossier sous windows.
3. Ne pas cocher la checkbox **Lecture seule** pour avoir les droits également en écriture.
4. Cocher **Montage automatique**.

![VirtualBox droit](img/virtualbox_droit.png)


#### Dans la machine vituelle Ubuntu

1. Ouvrir un terminal.
2. Éxecuter la commande le script ci-dessous:
```sh
# <nom-dossier-sous-windows>:
# tmp
#
# <chemin-absolu-machine-virtuelle-ubuntu>:
# /home/phyvm/virtualbox-tmp
#
# Si le dossier n'existe pas dans la machine virtuelle
# il faut le créer.
#
# sudo mount -t vboxsf -o uid=${UID},gid=$(id -g) <nom-dossier-sous-windows> <chemin-absolu-machine-virtuelle-ubuntu> 

if [[ ! -d ${HOME}/virtualbox-tmp ]]; then
  mkdir -p ${HOME}/virtualbox-tmp
fi

sudo mount -t vboxsf -o uid=${UID},gid=$(id -g) tmp ${HOME}/virtualbox-tmp
```
