Erreur
======

1. [Techoverflow.net: \[SOLVED\] Socket permission denied](https://techoverflow.net/2017/03/01/solving-docker-permission-denied-while-trying-to-connect-to-the-docker-daemon-socket/)

Socket permission denied
------------------------

```sh
docker: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock:
Post http://%2Fvar%2Frun%2Fdocker.sock/v1.26/containers/create: dial unix /var/run/docker.sock: connect: permission denied.
See 'docker run --help'.
```

### Fix: Socket permission denied

```sh
sudo usermod -a -G docker ${USER]
```

!!! Attention
    Obligation de se déloguer et reloguer pour appliqués tous les changements.
