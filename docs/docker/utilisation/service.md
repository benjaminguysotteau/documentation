Service
=======

docker-compose.yml
------------------

```yaml
version: "3"
services:
  web:
    # replace username/repo:tag with your name and image details
    image: username/repo:tag
    deploy:
      replicas: 5
      resources:
        limits:
          cpus: "0.1"
          memory: 50M
      restart_policy:
        condition: on-failure
    ports:
      - "4000:80"
    networks:
      - webnet
networks:
  webnet:
```

Gérer la surcharge
------------------

```sh
docker swarm init
```

```sh
docker stack deploy -c docker-compose.yml getstartedlab
```

```sh
docker service ls
```

```sh
docker service ps getstartedlab_web
```

```sh
docker container ls -q
```

Mettre l'application à l'échelle
--------------------------------

```sh
docker stack rm getstartedlab
```

```sh
docker swarm leave --force
```
