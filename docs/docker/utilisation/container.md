Containers
==========

Prérequis
---------

```sh
docker run hello-world
```

Construire une application
--------------------------

!!! example "Exemple"
    docker build -t <nom-de-mon-application\>

```sh
docker build -t friendlyhello
```

Lancer l'application
--------------------

```sh
docker run -p 4000:80 friendlyhello
```

Stopper une application
-----------------------

!!! example "Exemple"
    docker container stop <le-hash-de-l'application-en-cours-d'execution\>

```sh
docker container stop 1fa4ab2cf395
```

Partager une application
------------------------

### Identification

```sh
docker login
```

### Étiquetage

!!! example "Exemple"
    docker tag image <username\>/<repository\>:<tag\>

```sh
docker tag friendlyhello gordon/get-started:part2
```

### Publication

!!! example "Exemple"
    docker push <username\>/<repository>:<tag\>

### Récupérer et lancer une application distante

!!! example "Exemple"
    docker run -p 4000:80 <username\>/<repository\>:<tag\>

