Cluster
=======

Création
--------

```sh
docker-machine create --driver virtualbox myvm1
docker-machine create --driver virtualbox myvm2
```

Lister les containers et leurs IPs
----------------------------------

```sh
docker-machine ls
```

Initialiser un cluster et ajouter les noeuds
--------------------------------------------

```sh
docker-machine ssh myvm1 "docker swarm init --advertise-addr <myvm1 ip>"
docker-machine --native-ssh ssh myvm1 ...

docker-machine ssh myvm2 "docker swarm join \
--token <token> \
<ip>:2377"

docker-machine ssh myvm1 "docker node ls"
```

Déployer une application dans un cluster
----------------------------------------

!!! example "Exemple"
    docker-machine env <machine>

```sh
docker-machine env myvm1
eval $(docker-machine env myvm1)
```

Déployer une application dans le gestionnaire de cluster
--------------------------------------------------------

```sh
docker stack deploy
docker service ps <service_name>
docker stack deploy -c docker-compose.yml getstartedlab
docker login registry.example.com
docker stack deploy --with-registry-auth -c docker-compose.yml getstartedlab
docker stack ps getstartedlab
```

Accéder à un cluster
--------------------

Itérer et mettre l'application à l'échelle
------------------------------------------

Nettoyer et redémarrer
----------------------

```sh
docker stack rm getstartedlab
docker-machine ssh myvm2 "docker swarm leave"
docker-machine ssh myvm1 "docker swarm leave --force"
```

Supprimer les variables d'environnement docker-machine
------------------------------------------------------

```sh
eval $(docker-machine env -u)
```

Redémarrer docker-machine
-------------------------

```sh
docker-machine stop   <machine-name>
docker-machine start  <machine-name>
```
