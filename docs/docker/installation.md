Docker
======

!!! info
    Pour cette installation nous utiliserons la version gratuite de **Docker** qui est la **Community édition**.

Installation
------------

### Désinstaller les anciennes versions

```sh
sudo apt-get remove docker docker-engine docker.io
```

### Installer Docker Community édition

#### Installer les dépendances

```sh
sudo  apt-get update \
&&    apt-get install --install-recommends \
      apt-transport-https \
      ca-certificates \
      curl \
      software-properties-common
```

#### Ajouter la clé GPG officielle Docker

```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

#### Ajouter le dépôt aux sources d'APT

!!! info
    Ici nous ajoutons le dépôt réservé aux architectures processeurs x86_64 et amd64.

```sh
sudo  add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
      stable"
```

#### Installation Docker Community édition

```sh
sudo  apt-get update \
&&    sudo apt-get install docker-ce
```

#### Ajouter l'utilisation au groupe Docker

```sh
sudo usermod -a -G docker ${USER}
```

!!! Attention
    Obligation de se déloguer et reloguer pour appliqués tous les changements.

#### Tester le conteneur Hello-world

```sh
sudo docker run hello-world
```
