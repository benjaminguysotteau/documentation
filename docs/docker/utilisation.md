Utilisation
===========

Afficher les processus Dockerisés
---------------------------------

```sh
docker ps
```

!!! example "Exemple"
    | CONTAINER ID        | IMAGE                       | COMMAND                  | CREATED             | STATUS              | PORTS                    | NAMES         |
    | :------------------ | :-------------------------- | :----------------------- | :------------------ | :------------------ | :----------------------- | :------------ |
    | e194e9502ee8        | squidfunk/mkdocs-material   | "mkdocs serve --dev-…"   | 3 hours ago         | Up 3 hours          | 0.0.0.0:8001->8000/tcp   | gifted_easley |
