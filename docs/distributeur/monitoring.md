Monitoring
==========

Via Bash
--------

1. Ouvrir un emulateur de terminal
2. S'assurer d'etre dans le terminal **Bash**
3. Lancer la commande **lockovh**
4. Se rendre dans le dossier ${HOME}/log
3. Lancer la commande **alive**

Exemple:

192.168.0.*38*, le dernier numero IP est: **38**

