Flashage
========

Via Bash
--------

1. Ouvrir un emulateur de terminal
2. S'assurer d'etre dans le terminal **Bash**
3. Lancer la commande **lockto** `<dernier-numero-ip>`
4. Lancer la commande **sdcardto** `<dernier-numero-ip>`

Exemple:

192.168.0.*38*, le dernier numero IP est: **38**

Par le code
-----------

### Fonction lockto
```sh
$1=dernier_numero_ip

ssh root@192.168.0.$1
```

### Fonction sdcardto
```sh
$1=dernier_numero_ip

scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/firmware_micro_annexe/fuji_distri.0.2.17.mhx root@192.168.0.$1:/media/sdcard/updates/fuji_distri.0.2.17.mhx
```

Le micro annexe est sur la liaison serie 5.
