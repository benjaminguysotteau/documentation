Crash
=====

Synopsis
--------

En cas de crash d'un distributeur, on restaure sa version précédente.

Lors d'un crash le distributeur n'est plus accesible par ssh, il faut passer par sa centrale associée.

!!! info "distrib_inst.txt"
  Il faut penser à modifier le fichier distrib_inst.txt sur le serveur OVH
  pour modifier toutes les versions des distributeurs.

Procédure
---------

1. Connexion à [Optiweb](https://www.optiweb-access.fr/login) dans sa **version de production** (non beta).
2. Selection du client et de la **centrale associée**.
3. Obtention d'un port ssh pour la connection (reverse ssh), mes machines > paramêtres > **enable SSH port**.
4. Connection au serveur OVH depuis un post local.
```sh
# From Bash terminal with Alexandre Aliases.
lockovh
```

1. Connection à la centrale depuis OVH.
```sh
# From Bash terminal with Alexandre Aliases.
lockto $NUM_PORT_SSH
```

1. Connection au distributeur depuis sa centrale.
2. On s'assure qu'on est connecté a la bonne centrale.
```sh
ssh root@192.168.1.<num-ip>
cd /home/root
./who_are_you.sh
```

1. On consulte les **logs** pour voir les erreurs.
2. Dans le home du user de connection (different de /home/root/), on remplace le binaire courant par l'ancien binaire.
```sh
cd /home/log/
cat appliLog.txt
cat paramLog.txt
cd /home/
cp OLD_DISTRIBUTEUR DISTRIBUTEUR
```
1. On sort du distributeur puis de la centrale **(exit * 2)**.
2. On s'assure des changements depuis le serveur ovh.
```sh
cd /home/log
getname     # affiche le nom des distributeur par leur ville a la place du numero de distributeur.
getversion  # affiche la version qu'execute le distributeur.
alive       # affiche les distributeurs qui sont UP.
```
