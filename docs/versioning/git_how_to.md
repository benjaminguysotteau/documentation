Git How to
===========

Référence
---------

1. [à retrouver: GNU/Linux Magazine 119 - Git]()
2. [à retrouver: git.wiki.kernel.org GitSvnCrashCourse]()

Préquelle
---------

!!! objectif
    Nous parlerons des bonnes pratiques et des commandes à connaître.

!!! quote "Message"
    Tout ce qui suis est tiré de mon expérience avec git, que ce soit l'utilisation pour mes projets personnels ou professionnels,
    c'est lié à la façon dont on m'a appris à l'utiliser. Si vous avez une approche différente, n'hésitez pas à la partager.

Bonnes pratiques
----------------

Un commit doit être **atomique**.

??? question "Qu'est ce que ça veut dire ?"
    Un commit ne correspond qu'a **une seul fonctionnalité/modification**.

??? example "Exemple"
    Si on modifie du code pour y rajouter des **features sans lien entre elles**, il faut faire **plusieurs commits** différents, un par feature.

    Ça permet entre autre d'**intégrer seulement une partie d'un travail** lors d'un merge (**sans les commits à retravailler**)
    ou d'identifier plus facilement un commit à l'origine d'un bug (avec **git bisect**).

**Garder une branche** qui est une **copie du dépôt distant** (la branche master par exemple).

??? attention
    **On ne modifie pas du code dedans!**

    Elle sert de **source pour merger** notre travail avant de push sur le **dépôt distant**.

**On travail dans des branches**.

??? quote "Message"
    Une conséquence de la règle précédente, la master qui est la copie du dépôt distant et une (ou plusieurs) branche(s) pour travailler à côté.

    On fait généralement, une branche par grosse fonctionnalité (ou fonctionnalité distincte).
    Après on utilise **git rebase** pour merger le contenu des branches avant de le push (voir [rebase]()).

**Avant de push** une branche, on s'assure qu'elle est **à jour** par rapport au dépôt distant.

Tant qu'on a pas push, on **peut faire des modifications pour améliorer** ses commits.

??? example "Exemple"
    Rajouter des **modifications** qu'on avait **oublier**, **corriger** une petite erreur, **modifier** un message de commit, ...

    Et c'est très important, vu qu'on a la possibilité de faire les choses proprement avant de les mettre sur le dépôt distant, **IL FAUT LE FAIRE**!

Des **messages** de commits **explicitent** qui permettent de comprendre ce que fait celui-ci. 

??? info
    On laisse ensuite une ligne de vide avant d'ajouter plus détails sur le commit si nécessaire.

    Si on utilise vim pour éditer les messages de commit (voir plus bas, la variable **GIT_EDITOR**),
    la coloration syntaxique rappelle cette règle.

C'est valable pour **n'importe quel gestionnaire de version**, mais on ne push **JAMAIS** un code qui n'as **pas été testé** ou qui n'est **pas fonctionnel**!


Actions sur les fichiers: add, rm et mv
---------------------------------------

Ajouter dans le système de fichier, un fichier à l'emplacement du dépot local.

```sh
git add <nom-du-fichier>
```
 
Supprimer dans le système de fichier, un fichier à l'emplacement du dépôt local et distant.

```sh
git rm  <nom-du-fichier>
```

Supprimer dans le système de fichier, un fichier à l'emplacement du dépôt local.

```sh
git rm --cached <nom-du-fichier>
```

Evite de faire mv <nom-du-fichier> <destination> puis git rm <source> puis git add <destination>

```
git mv <nom-du-fichier> <destination>
```

Prise d'information: status, diff, log
--------------------------------------

Ensuite un trio de commandes que vous risquez de taper souvent:
Tout d'abord **git status**. Elle sert à voir l'état du repo local, la branche sur laquelle vous êtes, les fichiers concernés par le prochain commit,
ceux qui ne sont pas encore committé et ceux qui ne sont pas versionné; avec leur état (//new file//, //deleted//, //modified//, //both modified//, ...).
On l'utilise souvent avec **git diff** (la sortie est équivalente à un **diff -u**) pour voir la différence entre les fichiers committés et qui ne sont pas encore commit.
Et enfin la troisième très régulièrement utilisé, c'est ''git log'', pour voir les précedents commits. Vous pouvez utiliser l'option **--stat**
pour voir en plus les fichiers modifiés et le nombre de modifs: **git log --stat**.


Versionner les changements, les publier et les récupérer: commit, push et pull
------------------------------------------------------------------------------

**git commit** vous permet de commiter une (ou plusieurs) modification. Vous pouvez l'utiliser tel quel pour commit les modifications prête à être commit 
(partie //Changes to be committed// dans ''git status'') ou bien fournir un (ou plusieurs) fichier à commiter.
Vous avez également si besoin l'option **-a** qui permet de commiter tous les fichiers modifiés (la partie //Changes not staged for commit// de **git status**).
C'est une option à utiliser __uniquement__ quand c'est justifié ! N'oubliez pas que vos commits doivent être **atomique**.
Une commande parfois bien pratique pour ajouter quelque chose au dernier commit local: **git commit --amend**
Comme les commits sous git se font sur le repo local, pour les faire apparaître sur le repo publique, il vous faudra les push dessus.
On utilise **git push <repo distant> <branche ou commit que l'on veut push>**. Si l'on veut pusher sur une branche spécifique à partir d'une autre branche
(ça va être notre cas, on va push sur la master mais travailler dans d'autres branches): **git push <repo distant> <branche locale>:<branche distante>**
Pour récupérer les modifications d'une branche d'un repo distant on utilise **git pull <repo distant> [branche]**.
Si on ne précise pas de branche, on va récupérer l'intégralité du repo (donc toutes les branches).
Je le répète, avant chaque push, il convient de s'assurer d'être à jour par rapport à la version upstream.


Annuler une modification sur le repo local: reset
-------------------------------------------------

Il arrive parfois qu'on ai commencé des modifications en **local** et qu'on veuille les annuler. Pour celà, on a **git reset**.
Si vous avez par exemple ajouté le mauvais fichier pour le commit, vous ne pouvez pas faire un **git rm**, sinon vous allez supprimer le fichier du repo.
Vous pouvez faire **git reset** pour enlever les fichiers staged (partie //Changes to be committed// dans **git status**) et les replacer comme fichier non committé.
Vous pouvez aussi "dépiler des commits locaux" avec **git reset <commit jusqu'où dépiler>**. Toutes les modifications enregistré lors des commits ainsi "dépilé" se retrouve donc //not staged//.
Si on veut annuler des modifications et en plus faire en sorte qu'elle disparaissent (au lieu de passer //not staged//) on peut utiliser **git reset --hard <commit jusqu'où dépiler>**.
Il faut être très prudent quand on utilise cette option si on ne veux pas perdre des modifications.
Par expérience je vous recommande de créer une nouvelle branche copie de l'actuelle avant de jouer pour la première fois avec **--hard**,
ça vous évitera les mauvaises surprises (on est content d'avoir une branche en backup dans ce cas).
En relisant l'article [[http://www.unixgarden.com/index.php/administration-systeme/git-it|Git it!]],
j'ai vu qu'il y a une solution pour se rattraper avec le mot clé **ORIG_HEAD**: **git reset --hard ORIG_HEAD**
(attention quand même, les modifications qui n'ont pas été committé sont perdu si on utilise ''--hard'').


Découper une modif en plusieurs morceau: add -p
-----------------------------------------------

Ça arrive régulièrement d'ajouter plusieurs fonctionnalités en même temps dans un fichier, viens ensuite le problème du commit **atomique**.
Si on commit tous le fichier, ça ne sera pas un commit atomique. Heureusement, on peut ajouter morceau par morceau les modifications et ceux grâce à **git add -p <fichier>**
Avec cette option vous entrez dans un mode interactif où l'on vous demande quoi faire pour chaque morceau: **Stage this hunk [y,n,q,a,d,/,e,?]?**
On commence par faire '?' pour avoir l'aide (sinon ce n'est pas très explicite):

```sh
y - stage this hunk
n - do not stage this hunk
q - quit; do not stage this hunk nor any of the remaining ones
a - stage this hunk and all later hunks in the file
d - do not stage this hunk nor any of the later hunks in the file
g - select a hunk to go to
/ - search for a hunk matching the given regex
j - leave this hunk undecided, see next undecided hunk
J - leave this hunk undecided, see next hunk
k - leave this hunk undecided, see previous undecided hunk
K - leave this hunk undecided, see previous hunk
s - split the current hunk into smaller hunks
e - manually edit the current hunk
? - print help
```

Ensuite on comprend que on peu indiquer si l'on veux ajouter ce morceau (y) pour le prochain commit ou non (n), on peut le redécouper si besoin (s) et si ce n'est pas suffisant,
on peut éditer manuellement (e) pour indiquer quel partie garder.

Manipulation des branches: branch et checkout
---------------------------------------------

**git branch** sans argument vous montre les branches existantes ainsi que celle dans laquelle vous êtes. 
Pour créer une nouvelle branche copie de l'actuelle, il suffit de faire **git branch <nouvelle branche>**. Vous pouvez la supprimer avec **-d** ou la déplacer/renommer avec **-m**.
On créer généralement une nouvelle branche à partir de la branche master à jour.
Une fois une branche créée, il faut pouvoir en changer, on utilise pour ça **git checkout <branche>**. Un checkout demande à ce que toutes les modifications locales soit committés;
en effet contrairement à svn où les différentes branches sont accessibles en même temps dans le système de fichier, sous git vous ne voyez dans votre système de fichier que la branche actuelle.
D'où le besoin de commiter vos changements avant de changer de branche pour ne pas les perdre.

Mettre temporairement de coté une modification: stash
-----------------------------------------------------

L'obligation de commiter ses changements avant de changer de branche peut parfois être énervant surtout si l'on a pas fini de développer et donc qu'on n'as pas de quoi commit. 
C'est souvent le cas si l'on veut juste changer rapidement de branche pour mettre à jour la master par exemple.
On a deux possibilité, soit faire un commit en local et le reset après, ou alors utiliser **git stash** qui va mettre les modifications non committé de coté pour pouvoir les appliquer plus tard.
Stash marche comme une pile, à chaque fois que vous "stasher" un nouveau contenu il est empilé et vous pouvez le dépiler avec **git stash pop**,
ce qui aura pour effet de ré-appliquer les modifications que vous aviez mises de coté. Vous pouvez ajouter un message au stash si vous voulez gérer plus facilement différent stash:
**git stash save "message"**


Mettre à jour des branches entre elles: rebase
----------------------------------------------

Après avoir travaillé dans une branche, arrive le moment où l'on veut fusionner son travail avec une autre branche (souvent la master qui contient le travail des autres développeur).
Git propose un mécanisme très pratique, le rebase. La commande **git rebase <branche>** va vous permettre de prendre la branche spécifiée en paramètre et va y rejouer les commits de la branche actuelle.
Le tout sans dupliquer les éventuels commit commun aux deux branches et en gérant au mieux les conflits.
Dans le cas où git ne sais pas résoudre un conflit par lui même, il vous demandera de le résoudre à la main et de continuer le rebase avec **git rebase --continue**
Pour mieux comprendre le fonctionnement de rebase, je vous renvoi à sa page manuel (appuyé par des schémas) et au petit exemple à la fin de ce how to.

Récupérer un commit précis dans une branche: cherry-pick
--------------------------------------------------------

Il arrive par moment que l'on ai besoin de récupérer un commit bien particulier dans une branche (par exemple la correction d'un bug). Pour ça, on peut utiliser **git cherry-pick <commit>**

Modifier l'existant (en local): rebase -i
-----------------------------------------

Une des possibilités très puissante offerte par la travail en branche est le rebase interactif.
Ce dernier vous permet de modifier tous une série de commit ajoutés par votre branche par rapport à une autre (la master par exemple).
Quand vous entrez en mode interactif, vous allez avoir tous les commits d'affichés, du plus ancien au plus récent, avec une action à appliquer dessus. Les actions possibles sont les suivantes:

```sh
# Commands:
#  p, pick = use commit
#  r, reword = use commit, but edit the commit message
#  e, edit = use commit, but stop for amending
#  s, squash = use commit, but meld into previous commit
#  f, fixup = like "squash", but discard this commit's log message
#  x, exec = run command (the rest of the line) using shell
```

Vous pouvez choisir ainsi de garder ou non un commit (il suffit de supprimer la ligne pour supprimer le commit),
modifier le message de commit (si vous avez fait une erreur ou si vous voulez apporter des précisions),
merger deux commit ensemble (en conservant ou non le message de commit) voir changer l'ordre (il suffit de déplacer la ligne du commit).
Par exemple, vous êtes en train de faire une série de modification, vous avez disons 5 commits.
Vous vous rendez compte que le commit 2 et 4 concerne la même fonctionnalité et n'ont aucun impact sur le commit 3 (pour éviter les conflits).
Dans ce cas, vous pouvez fusionner les deux commits en un seul grâce à la commande squash lors du rebase interactif.
Vous vous apercevez également que vous avez oublié de mentionner quelque chose dans le commit 1 et que vous avez oubliez un ';' dans le code du commit 3.
Il vous suffit par exemple pour corriger l'oublie du ';' de faire un nouveau commit et de le placer en fixup après le commit 3.

Lorsque vous lancer le rebase -i, vous obtenez le texte suivant:

```sh
pick f46a11a Commit 1
pick a8439fb Commit 2
pick 053178e Commit 3
pick b21ffb5 Commit 4
pick 379318a Commit 5
pick 9d8e133 Fix commit 3

# Rebase 5ea10dc..9d8e133 onto 5ea10dc
```

Après les modifications énoncé plus haut:

```sh
reword f46a11a Commit 1
pick a8439fb Commit 2
squash b21ffb5 Commit 4
pick 053178e Commit 3
fixup 9d8e133 Fix commit 3
pick 379318a Commit 5

# Rebase 5ea10dc..9d8e133 onto 5ea10dc
```

Une fois que vous aurez sauvegarder vos modifs, git va commencer son rebase. Il va vous demander ce qu'il y a à changer dans le message du premier commit,
il va prendre le deuxième commit tel quel et le fusionner avec le quatrième (en gardant les deux messages de commit).
Puis il va prendre le troisième commit et va le fusionner avec votre fix (sans garder le message "Fix commit 3"). Enfin il prendra le cinquième commit.
Évidement, comme pour un rebase classique, s'il y a des conflits vous devrez les résoudre à la main avant de continuer (avec ''git rebase --continue'').
C'est dans ce genre de cas que l'on comprend l'intérêt de faire des commits atomique, ça permet de ne choisir que certains éléments sans avoir des conflits
ou des problèmes de dépendance trop important liés aux autres commits.

Générer une série de patchs: format-patch
-----------------------------------------

Il peut être pratique de générer des patchs à partir de ses commits. Par exemple, dans le cas où l'on à pas de repo publique à soit
et que l'on veut faire tester/vérifier une modification à quelqu'un sans la push sur un repo publique commun. Pour cela, on utilise ''git format-patch <branche|commit|range de commit>''.
On peut ensuite envoyer le(s) patch(s) généré(s) à quelqu'un qui pourra les appliquer avec ''git am <patch>''

Le manuel à tout moment: help
-----------------------------

Pour avoir le manuel d'une sous commande à n'importe quel moment, il suffit de lui rajouter l'option **--help** ou d'utiliser **git help <sous commande>**
Au passage, pour voir toutes les manières d'identifier un commit (ou un range de commit): **man gitrevisions**


Configuration
-------------

Vous pouvez modifier certaines variables d'environnement pour modifier les programmes extérieur qu'utilisera git:
**GIT_EDITOR** pour savoir dans quel éditeur ouvrir les messages de commit, les rebases interactif, l'ajout interactif, ...
**PAGER** pour ouvrir les différentes pages manuel de git, les logs, les diffs, ...
N'oubliez pas aussi de modifier votre fichier **~/.gitconfig** pour que votre nom apparaissent correctement dans les messages de commit
et pour avoir la couleur (très pratique, surtout pour les diff et les status).

```sh
[user]
    name = Prénom Nom
    email = mon@mail.org
[color]
    diff = auto
    status = auto
    branch = auto
```


Régler un conflit
-----------------

Il arrive lorsqu'on est plusieurs à modifier le même code (même lorsqu'on est seul...) qu'il y ai des conflits à résoudre manuellement.

Quand ça arrive, on a un message du genre:

```sh
Falling back to patching base and 3-way merge...
Auto-merging mon_fichier_en_conflit$
CONFLICT (content): Merge conflict in mon_fichier_en_conflit
Failed to merge in the changes.
Failed to merge in the changes.
Patch failed at 0001 Message de commit
```

On vous donne le nom du fichier en conflit dont il va falloir vous occuper. Vous pouvez également le voir avec git status:

```sh
both modified:      mon_fichier_en_conflit
```

Dans cet exemple, il s'agit d'un conflit lors d'un rebase, le même fichier à été modifié au même endroit dans les deux branches.

Armez vous de votre éditeur favori et ouvrer le fichier. Vous allez vous retrouvez en face de ce genre de chose:

```sh
<<<<<<< HEAD
Contenu de la branche master
=======
Contenu de ma branche dev
>>>>>>> Message de commit
```

Vous avez sous les yeux les deux contenus qui rentre en conflit, il faut faire un choix pour ne garder que celui qui vous intéresse.
Une fois votre choix fait, vous pouvez supprimer les chevrons **<nowiki><<<<<<<</nowiki>**, **<nowiki>=======</nowiki>** et **<nowiki>>>>>>>></nowiki>**
(ainsi que le code en conflit que vous n'avez pas retenu bien sur) et enregistrer votre fichier.

Il faut ensuite signaler à git que vous avez résolu le conflit avec:

```sh
git add mon_fichier_en_conflit
```

Vous pouvez voir avec git status qu'il change d'état:

```sh
modified:   mon_fichier_en_conflit
```

Ensuite il ne vous reste qu'à terminer ce que vous étiez en train de faire, généralement git vous donne la commande à utiliser après avoir résolu un conflit,

par exemple pour un **git rebase** il nous affiche:

```sh
When you have resolved this problem run "git rebase --continue"
If you would prefer to skip this patch, instead run "git rebase --skip"
To check out the original branch and stop rebasing run "git rebase --abort"
```

```
On va donc faire un **git rebase --continue** pour terminer notre rebase.
```


Exemple concret
---------------


Je terminerais par un petit exemple:
Vous êtes sur votre branche dev avec une modification en cours qui n'est pas encore committé. Votre branche master contient une copie du repo publique.
Pour pouvoir tester les modifications que vous êtes en train de faire, vous avez besoin des derniers commit qui ont été push sur le repo publique.
Comment procéder ?

Tout d'abord vous allez mettre temporairement vos modifications de coté pour pouvoir changer de branche sans être obligé de commit:

```sh
git stash
```

Ensuite vous changer de branche pour la master et vous récupérer le contenu à jour du repo publique:

```sh
git checkout master
git pull origin master
```

Vous retournez ensuite sur votre branche dev et vous allez la rebase par rapport à la branche master:

```sh
git checkout dev
git rebase master
```

Maintenant que vous avez votre branche dev à jour avec les derniers commit présent sur le repo publique, il vous suffit de remettre en place les modifications que vous aviez mis de coté:

```sh
git stash pop
```

Ensuite il vous reste à terminer vos modifications, tester votre code et le commit.
Une fois que c'est fait, il reste à le push sur le repo publique. Avant de push, on s'assure d'être à jour avec le repo distant,
on va donc retourner sur la branche master pour pull et faire un rebase dans la branche dev s'il y a eu des modifications:

```sh
git checkout master
git pull origin master
git checkout dev
git rebase master
```

Une fois qu'on est sur d'avoir notre branche à jour, il ne reste plus qu'à la push dans la branche master du repo distant:

```sh
git push origin dev:master
```
