Checkout
========

Checkout sur commit
-------------------

```sh
git stash list
git stash # Si on a effectue des changements.
git checkout f111db738b93a56f550d5d6e10e3216a80312551
# Si l'on fait des changements depuis ce commit
git stash
git checkout feature/hopper-jeton-0.1.0
git stash list
stash@{0} # le commit qui contient les changement pendant le checkout.
stash@{1} # le commit qui contient les modifications avant le checkout sur commit.
git stash pop   stash@{1}
git stash drop  stash@{0}
```
