Git Flow
========

Initialisation
--------------

1. Démarrer un projet **git-flow**, à la **racine** du projet dans la branche **master**.
2. Exécuter la commande suivante:

```sh
git flow init
```

Laisser les paramêtres par défaut en appuyant sur la touche **entrée** pour toutes les questions.

Feature
-------

```sh
# Création d'une nouvelle feature.
# git flow feature start <my-feature-0.1.0>
git flow feature start submodule-0.1.0

# Une fois le code terminé,
# fermer une feature pour l'ajouter au dépôt.
# git flow feature finish <my-feature-0.1.0>
git flow feature finish submodule-0.1.0
```

Release
-------

```sh
# Création d'une nouvelle release.
# git flow release start <my-release-0.1.0>
git flow feature start documentation-0.1.1

# Une fois le code terminé,
# fermer une release pour l'ajouter au dépôt.
# git flow feature finish <my-release-0.1.0>
git flow feature finish documentation-0.1.1
```

Tag
---

Lorsqu'une **release** a été fermé, on peut push sur le dépôt distant, le **tag** qui correspond à cette release.

```sh
git push --tags
```

Publish
-------

```sh
git stash list
git stash
git flow feature publish hopper-0.1.0

# Pour récupérer la branche qui à été publié.
git checkout master # le fetch ne fonctionne que depuis master
git fetch
git checkout feature/hopper-0.1.0
```

Référence
---------

1. [Git flow chart](https://danielkummer.github.io/git-flow-cheatsheet/)
