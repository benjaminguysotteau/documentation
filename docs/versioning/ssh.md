Connection SSH
==============

Test connexion SSH
------------------

Tester sa connexion au serveur bitbucket:

```sh
ssh -T git@bitbucket.org

# On obtient:

logged in as benjaminguysotteau.

You can use git or hg to connect to Bitbucket. Shell access is disabled.
```

Démarrer un agent SSH
---------------------

```sh
eval "$(ssh-agent -s)"

# On obtient par exemple:

Agent pid 19702
```

Tuer un agent SSH
-----------------

```sh
ssh-agent -k
```

Lister les clés SSH en cours d'utilisation
------------------------------------------

```sh
ssh-add -l

# Lors que toutes mes clés sont ajoutées:

4096 f2:1c:c8:a6:d6:5b:b2:45:5c:ab:31:c6:b1:e0:ad:70 /home/phyvm/.ssh/vm_phyvm_bgs_attlasian_bitbucket (RSA)
4096 70:9c:45:b4:64:22:69:10:33:02:3e:0b:8c:d4:7f:37 /home/phyvm/.ssh/vm_phyvm_bgs_bitbucket_heurtaux (RSA)
4096 70:9c:45:b4:64:22:69:10:33:02:3e:0b:8c:d4:7f:37 benjamin.guysotteau@heurtaux.fr (RSA)
4096 f2:1c:c8:a6:d6:5b:b2:45:5c:ab:31:c6:b1:e0:ad:70 benjamin.guysotteau@heurtaux.fr (RSA)
```

Supprimer les clés SSH en cours d'utilisation
---------------------------------------------

```sh
ssh-add -D

# On obtient:

All identities removed.

# ssh-add -l:

4096 70:9c:45:b4:64:22:69:10:33:02:3e:0b:8c:d4:7f:37 benjamin.guysotteau@heurtaux.fr (RSA)
4096 f2:1c:c8:a6:d6:5b:b2:45:5c:ab:31:c6:b1:e0:ad:70 benjamin.guysotteau@heurtaux.fr (RSA)
```
