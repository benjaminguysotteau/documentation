Git
===

Créer une etiquette
-------------------

### Référence

1. [Git documentaion étiquetage](https://git-scm.com/book/fr/v1/Les-bases-de-Git-%C3%89tiquetage)
2. [Sushi Hangover rénommer etiquette](http://sushihangover.github.io/git-renaming-a-got-tag/)

!!! info
    L'option **-a** permet de spécifier le nom de l'étiquette.

    L'option **-m** permet de spécifier le message d'étiquetage qui sera stocké avec l'étiquette.

```sh
git tag -a distributeur-1.2.8 -m "distributeur en version 1.28"
git push --tags
```

Créer une étiquette pour version précédente
-------------------------------------------

```sh
git checkout 5d6f0542d8bf3244e34f1932c03632b93063a152
git tag -a distributeur-1.2.8 -m "distributeur en version 1.28"
git push --tags
```

Renommer une étiquette
----------------------

Pour renommer une étiquette en local et pusher les modifications sur le dépôt distant.

!!! attention
    Pour **git tag -m** il faut bien mettre en **premier** le nom du **nouveau tag**,

    puis en **second**, le nom de l'ancien tag.

```sh
git tag -m '<new-tag>' '<old-tag>'

# Supprimer un tag: ancienne méthode.
git push origin :refs/tags/<old-tag>

# Supprimer un tag: nouvelle méthode.
git push origin --delete <old-tag>

git tag -d <old-tag> 
git push --tags
```

!!! bug "Git version 1.9.1"
    Lorsque que l'on fait **git tag -d <old-tag\>** AVANT **git push origin :refs/tags/<old-tag\>**.

    Lors du **git push origin --tags**, les anciens tags sont de nouveaux pushés!

Supprimer une étiquette
------------------------

```sh
# En local
# git tag -d <tag-name>
git tag -d 0.1.1

# Pour le remote
# git push <remote> --delete <tag-name>
git push --delete origin 0.1.1

# Appliquer les changements
git push --tags
```

Supprimer une branche locale
----------------------------

```sh
git branch -d <nom-de-la-branche>
```

!!! attention
    Pour une branche qui n'a pas été complètement mergée.

```sh
git branch -D <nom-de-la-branche>
```

Supprimer une branche distante
------------------------------

```sh
# Ancienne méthode
git push origin :<nom-de-la-branche\>

# Nouvelle méthode
git push origin --delete <nom-de-la-branche\>
```
