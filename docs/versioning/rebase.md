Rebase (Remisage)
=================

Rebase intéractif
-----------------

Pour rebase 2 commits.

```sh
git rebase -i HEAD~2
```

Configurer la branche à tracker
-------------------------------

```sh
# git branch --set-upstream-to=<remote>/<branch> <branch>
git branch --set-upstream-to=origin/master feature/refactoring-0.1.0 
```

Procédure
---------

```sh
git branch --set-upstream-to=origin/master feature/refactoring-0.1.0
git stash list
git stash
git rebase -i
git stash pop
```

Conflit
-------

!!! attention
    Lors d'un **conflit** suite à un **rebase intéractif** comme dans le message ci-dessous.

    Il faut **ouvrir le fichier** qui est modifié par les deux versions différentes et

    **conserver seulement la partie qui nous intéresse** en supprimant le code de l'autre version.

```sh
rebase in progress; onto f111db7
You are currently rebasing branch 'feature/hopper-jeton-0.1.0' on 'f111db7'.
  (fix conflicts and then run "git rebase --continue")
  (use "git rebase --skip" to skip this patch)
  (use "git rebase --abort" to check out the original branch)

Unmerged paths:
  (use "git reset HEAD <file>..." to unstage)
  (use "git add <file>..." to mark resolution)

	both modified:      ecran_demarrage.cpp

no changes added to commit (use "git add" and/or "git commit -a")
```

!!! attention
    Une fois qu'on a supprimé le code dont on ne veut pas.

```sh
ecran_demarrage.cpp: needs merge
You must edit all merge conflicts and then
mark them as resolved using git add
```

```sh
git add ecran_demarrage.cpp
git rebase --continue
```

