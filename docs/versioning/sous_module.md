Sous module
===========

Ajouter une dépendance à un projet
----------------------------------

```sh
# A la racine du projet principale,
# git submodule add <addresse-du-depot-en-ssh-ou-https>
git submodule add https://benjaminguysotteau@bitbucket.org/heurtauxsas/libsh.git

git commit -m "projet: ajout du projet libsh en dépendance"
```

Cloner un projet et ses sous modules
------------------------------------

On peut cloner un projet ainsi que ses dépôts en dépendances directement en ajoutant l'argument **--recurse-modules**.

```sh
git clone --recurse-submodules https://bitbucket.org/heurtaux-sas/<nom-depot>
```

Cloner uniquement les dépendances
---------------------------------

Pour un dépôt déjà cloné, se rendre dans le **dossier vide** qui porte le nom du dépôt distant puis faire les commandes suivantes:

```sh
cd /path/to/submodule/repository

git submodule init

git submodule update
```

Mettre à jouer les dépendances
------------------------------

Pour un projet déjà éxistant, à la racine du **dépot principale**, éxécuter la commande suivante:

```sh
git submodule update --recursive --remote
```
