Installation
============

!!! danger
    Suivre ces étapes uniquement après flasher le **module PHYTEC**.

    [01 - Préparation](preparation.md)

00 - Config-expansions
----------------------

Pour la gestion de l'écran en 10 pouces.

```sh
# Retourner à la racine de PTXdist.
cd /env/
```

Éditer le fichier **config-expansions** à l'aide du binaire <ins>edit</ins>:

??? info "Sauvegarde sous edit"
    **edit** est hérité de nano, <ctrl-d\> pour sauvegarder et quitter.

```sh
# /env
edit config-expansions
```

Et ajouter les deux lignes suivantes à la fin du fichier.

```sh
#10.4" display AMPIRE HX
of_display_timings -S /panel/display-timings/AM800600
```

Puis sauvegarder.

01 - Sauvegarder config-expansions
----------------------------------

!!! attention
    Impérativement faire un **saveenv** pour enregistrer les modifications!

**Sauvegarder** les **modifications effectuées** avec la commande suivante:

```sh
saveenv

reset
# On patiente jusqu'à l'affichage du prompt.
# Cette fois-ci on n'appuie pas sur la touche entree pour stopper le décompte,
```

??? info "Résultat sortie standard"

    ```sh

    barebox@Phytec phyCORE AM335x:/env reset
    
    
    barebox 2015.09.0-AM335x-PD15.2.1 #1 Thu Jan 14 19:05:05 CET 2016
    
    
    Board: Phytec phyCORE AM335x
    nand: ONFI flash detected
    nand: NAND device: Manufacturer ID: 0x2c, Chip ID: 0xdc (Micron MT29F4G08ABADAH4), 512MiB, page size: 2048, OOB size: 64
    booting from NAND
    
    
    barebox 2015.09.0-AM335x-PD15.2.1 #1 Fri Dec 22 14:31:26 CET 2017
    
    
    Board: Phytec phyCORE AM335x
    cpsw 4a100000.ethernet: detected phy mask 0x1
    mdio_bus: miibus0: probed
    eth0: got preset MAC address: b0:d5:cc:cc:d9:4a
    am335x-phy-driver 47401b00.usb-phy: am_usbphy 8ff20a24 enabled
    musb-hdrc: ConfigData=0xde (UTMI-8, dyn FIFOs, bulk combine, bulk split, HB-ISO Rx, HB-ISO Tx, SoftConn)
    musb-hdrc: MHDRC RTL version 2.0 
    musb-hdrc: setup fifo_mode 4
    musb-hdrc: 28/31 max ep, 16384/16384 memory
    m25p80 m25p80@00: s25sl064p (8192 Kbytes)
    i2c-omap 44e0b000.i2c: bus 0 rev0.11 at 400 kHz
    omap-hsmmc 48060000.mmc: registered as 48060000.mmc
    mmc0: detected SD card version 2.0
    mmc0: registered mmc0
    OMAP Watchdog Timer Rev 0x01
    nand: ONFI flash detected
    nand: NAND device: Manufacturer ID: 0x2c, Chip ID: 0xdc (Micron MT29F4G08ABADAH4), 512MiB, page size: 2048, OOB size: 64
    netconsole: registered as netconsole-1
    malloc space: 0x8ff00000 -> 0x9fdfffff (size 255 MiB)
    barebox-environment environment-nand.4: setting default environment path to /dev/nand0.bareboxenv.bb
    running /env/bin/init...
    ******************************************************
    ** Distributeur   --- CENTERPAY ---                 **
    ******************************************************
    
    Stopper autoboot empeche auto update du kernel et rootfs
    
    Hit m for menu or any other key to stop autoboot:  0
     Montage SD et verif si script yocto_sysupdate present 
    yocto_sysupdate non present
    blspec: blspec_scan_directory: nand loader/entries
    
    Loading ARM Linux zImage '/dev/nand0.kernel.bb'
    Loading devicetree from '/dev/nand0.oftree.bb'
    commandline: consoleblank=0 console=ttyO0,115200n8  root=ubi0:root ubi.mtd=root rootfstype=ubifs rw
    [    0.000000] Booting Linux on physical CPU 0x0
    [    0.000000] Linux version 3.12.30-AM335x-PD15.2.1 (phyvm@so530v4) (gcc version 4.9.2 (GCC) ) #1 SMP Fri Mar 25 08:30:19 CET 2016
    [    0.000000] CPU: ARMv7 Processor [413fc082] revision 2 (ARMv7), cr=10c5387d
    [    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
    [    0.000000] Machine: Generic AM33XX (Flattened Device Tree), model: Phytec AM335x PCM-953
    [    0.000000] cma: CMA: reserved 24 MiB at 9e000000
    [    0.000000] Memory policy: ECC disabled, Data cache writeback
    [    0.000000] CPU: All CPU(s) started in SVC mode.
    [    0.000000] AM335X ES2.1 (sgx neon )
    [    0.000000] PERCPU: Embedded 8 pages/cpu @c0d4a000 s9024 r8192 d15552 u32768
    [    0.000000] Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 129280
    [    0.000000] Kernel command line: consoleblank=0 console=ttyO0,115200n8  root=ubi0:root ubi.mtd=root rootfstype=ubifs rw
    [    0.000000] PID hash table entries: 2048 (order: 1, 8192 bytes)
    [    0.000000] Dentry cache hash table entries: 65536 (order: 6, 262144 bytes)
    [    0.000000] Inode-cache hash table entries: 32768 (order: 5, 131072 bytes)
    [    0.000000] Memory: 482488K/521216K available (6083K kernel code, 579K rwdata, 2060K rodata, 396K init, 259K bss, 38728K reserved, 0K highmem)
    [    0.000000] Virtual kernel memory layout:
    [    0.000000]     vector  : 0xffff0000 - 0xffff1000   (   4 kB)
    [    0.000000]     fixmap  : 0xfff00000 - 0xfffe0000   ( 896 kB)
    [    0.000000]     vmalloc : 0xe0800000 - 0xff000000   ( 488 MB)
    [    0.000000]     lowmem  : 0xc0000000 - 0xe0000000   ( 512 MB)
    [    0.000000]     pkmap   : 0xbfe00000 - 0xc0000000   (   2 MB)
    [    0.000000]     modules : 0xbf000000 - 0xbfe00000   (  14 MB)
    [    0.000000]       .text : 0xc0008000 - 0xc07fc04c   (8145 kB)
    [    0.000000]       .init : 0xc07fd000 - 0xc0860340   ( 397 kB)
    [    0.000000]       .data : 0xc0862000 - 0xc08f2d88   ( 580 kB)
    [    0.000000]        .bss : 0xc08f2d90 - 0xc0933994   ( 260 kB)
    [    0.000000] Hierarchical RCU implementation.
    [    0.000000]  RCU restricting CPUs from NR_CPUS=2 to nr_cpu_ids=1.
    [    0.000000] NR_IRQS:16 nr_irqs:16 16
    [    0.000000] IRQ: Found an INTC at 0xfa200000 (revision 5.0) with 128 interrupts
    [    0.000000] Total of 128 interrupts on 1 active controller
    [    0.000000] OMAP clockevent source: timer2 at 25000000 Hz
    [    0.000000] sched_clock: 32 bits at 25MHz, resolution 40ns, wraps every 171798ms
    [    0.000000] OMAP clocksource: timer1 at 25000000 Hz
    [    0.000000] Console: colour dummy device 80x30
    [    0.000433] Calibrating delay loop... 398.13 BogoMIPS (lpj=1990656)
    [    0.049581] pid_max: default: 32768 minimum: 301
    [    0.049751] Security Framework initialized
    [    0.049835] Mount-cache hash table entries: 512
    [    0.065712] CPU: Testing write buffer coherency: ok
    [    0.066241] CPU0: thread -1, cpu 0, socket -1, mpidr 0
    [    0.066318] Setting up static identity map for 0xc05f6308 - 0xc05f6378
    [    0.067383] Brought up 1 CPUs
    [    0.067401] SMP: Total of 1 processors activated.
    [    0.067413] CPU: All CPU(s) started in SVC mode.
    [    0.068302] devtmpfs: initialized
    [    0.074186] VFP support v0.3: implementor 41 architecture 3 part 30 variant c rev 3
    [    0.143201] omap_hwmod: debugss: _wait_target_disable failed
    [    0.144213] pinctrl core: initialized pinctrl subsystem
    [    0.145525] regulator-dummy: no parameters
    [    0.148393] NET: Registered protocol family 16
    [    0.151843] DMA: preallocated 256 KiB pool for atomic coherent allocations
    [    0.156245] cpuidle: using governor ladder
    [    0.156268] cpuidle: using governor menu
    [    0.167579] platform mpu.2: FIXME: clock-name 'fck' DOES NOT exist in dt!
    [    0.169304] platform 49000000.edma: FIXME: clock-name 'fck' DOES NOT exist in dt!
    [    0.171377] OMAP GPIO hardware version 0.1
    [    0.189128] omap-gpmc 50000000.gpmc: could not find pctldev for node /pinmux@44e10800/pinmux_nandflash, deferring probe
    [    0.189174] platform 50000000.gpmc: Driver omap-gpmc requests probe deferral
    [    0.190194] platform 56000000.sgx: FIXME: clock-name 'fck' DOES NOT exist in dt!
    [    0.196395] DSS not supported on this SoC
    [    0.196423] No ATAGs?
    [    0.196438] hw-breakpoint: debug architecture 0x4 unsupported.
    [    0.199225] cpsw.1: No hwaddr in dt. Using b0:d5:cc:cc:d9:4c from efuse
    [    0.245328] bio: create slab <bio-0> at 0
    [    0.271473] edma-dma-engine edma-dma-engine.0: TI EDMA DMA engine driver
    [    0.272834] vcc5v: 5000 mV 
    [    0.273415] vcc3v3: 3300 mV 
    [    0.273938] vcc1v8: 1800 mV 
    [    0.274473] lcd_3v3: 3300 mV 
    [    0.281411] SCSI subsystem initialized
    [    0.283653] usbcore: registered new interface driver usbfs
    [    0.283955] usbcore: registered new interface driver hub
    [    0.284305] usbcore: registered new device driver usb
    [    0.285877] omap_i2c 44e0b000.i2c: could not find pctldev for node /pinmux@44e10800/pinmux_i2c0, deferring probe
    [    0.285917] platform 44e0b000.i2c: Driver omap_i2c requests probe deferral
    [    0.286329] media: Linux media interface: v0.10
    [    0.286614] Linux video capture interface: v2.00
    [    0.287063] pps_core: LinuxPPS API ver. 1 registered
    [    0.287078] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
    [    0.287309] PTP clock support registered
    [    0.292505] Switched to clocksource timer1
    [    0.325079] NET: Registered protocol family 2
    [    0.326312] TCP established hash table entries: 4096 (order: 3, 32768 bytes)
    [    0.326425] TCP bind hash table entries: 4096 (order: 3, 32768 bytes)
    [    0.326523] TCP: Hash tables configured (established 4096 bind 4096)
    [    0.326610] TCP: reno registered
    [    0.326630] UDP hash table entries: 256 (order: 1, 8192 bytes)
    [    0.326663] UDP-Lite hash table entries: 256 (order: 1, 8192 bytes)
    [    0.326979] NET: Registered protocol family 1
    [    0.327506] RPC: Registered named UNIX socket transport module.
    [    0.327524] RPC: Registered udp transport module.
    [    0.327535] RPC: Registered tcp transport module.
    [    0.327545] RPC: Registered tcp NFSv4.1 backchannel transport module.
    [    0.328056] NetWinder Floating Point Emulator V0.97 (double precision)
    [    0.328218] hw perfevents: enabled with ARMv7 Cortex-A8 PMU driver, 5 counters available
    [    0.329353] PM: Loading am335x-pm-firmware.bin
    [    0.556217] VFS: Disk quotas dquot_6.5.2
    [    0.556421] Dquot-cache hash table entries: 1024 (order 0, 4096 bytes)
    [    0.557350] NFS: Registering the id_resolver key type
    [    0.557449] Key type id_resolver registered
    [    0.557464] Key type id_legacy registered
    [    0.557519] jffs2: version 2.2. (NAND) (SUMMARY)  © 2001-2006 Red Hat, Inc.
    [    0.558381] msgmni has been set to 990
    [    0.561199] NET: Registered protocol family 38
    [    0.561260] io scheduler noop registered
    [    0.561274] io scheduler deadline registered
    [    0.561308] io scheduler cfq registered (default)
    [    0.564736] pinctrl-single 44e10800.pinmux: 142 pins at pa f9e10800 size 568
    [    0.576832] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
    [    0.581030] 44e09000.serial: ttyO0 at MMIO 0x44e09000 (irq = 88, base_baud = 3000000) is a OMAP UART0
    [    1.247393] console [ttyO0] enabled
    [    1.252241] 48022000.serial: ttyO1 at MMIO 0x48022000 (irq = 89, base_baud = 3000000) is a OMAP UART1
    [    1.263177] 48024000.serial: ttyO2 at MMIO 0x48024000 (irq = 90, base_baud = 3000000) is a OMAP UART2
    [    1.274199] 481a6000.serial: ttyO3 at MMIO 0x481a6000 (irq = 60, base_baud = 3000000) is a OMAP UART3
    [    1.285932] 481a8000.serial: ttyO4 at MMIO 0x481a8000 (irq = 61, base_baud = 3000000) is a OMAP UART4
    [    1.296891] 481aa000.serial: ttyO5 at MMIO 0x481aa000 (irq = 62, base_baud = 3000000) is a OMAP UART5
    [    1.308982] omap_rng 48310000.rng: OMAP Random Number Generator ver. 20
    [    1.316768] [drm] Initialized drm 1.1.0 20060810
    [    1.339644] brd: module loaded
    [    1.352878] loop: module loaded
    [    1.365693] edma-dma-engine edma-dma-engine.0: allocated channel for 0:17
    [    1.373577] edma-dma-engine edma-dma-engine.0: allocated channel for 0:16
    [    1.381339] m25p80 spi1.0: found s25sl064p, expected m25p80
    [    1.387604] m25p80 spi1.0: s25sl064p (8192 Kbytes)
    [    1.392813] 5 ofpart partitions found on MTD device spi1.0
    [    1.398604] Creating 5 MTD partitions on "spi1.0":
    [    1.403733] 0x000000000000-0x000000020000 : "xload"
    [    1.411230] 0x000000020000-0x0000000a0000 : "barebox"
    [    1.418843] 0x0000000a0000-0x0000000c0000 : "bareboxenv"
    [    1.426734] 0x0000000c0000-0x0000000e0000 : "oftree"
    [    1.434315] 0x0000000e0000-0x000000800000 : "kernel"
    [    1.447866] CAN device driver interface
    [    1.454337] c_can_platform 481cc000.d_can: c_can_platform device registered (regs=fa1cc000, irq=68)
    [    1.467464] usbcore: registered new interface driver asix
    [    1.473750] usbcore: registered new interface driver ax88179_178a
    [    1.480486] usbcore: registered new interface driver cdc_ether
    [    1.487137] usbcore: registered new interface driver r815x
    [    1.493380] usbcore: registered new interface driver smsc95xx
    [    1.499725] usbcore: registered new interface driver net1080
    [    1.506008] usbcore: registered new interface driver cdc_subset
    [    1.512614] usbcore: registered new interface driver zaurus
    [    1.518817] usbcore: registered new interface driver cdc_ncm
    [    1.525748] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
    [    1.532728] ehci-pci: EHCI PCI platform driver
    [    1.538009] usbcore: registered new interface driver cdc_wdm
    [    1.544361] usbcore: registered new interface driver usb-storage
    [    1.556606] musb-hdrc musb-hdrc.0.auto: Enabled SW babble control
    [    1.563526] musb-hdrc musb-hdrc.0.auto: Falied to request rx1.
    [    1.569723] musb-hdrc musb-hdrc.0.auto: musb_init_controller failed with status -517
    [    1.578133] platform musb-hdrc.0.auto: Driver musb-hdrc requests probe deferral
    [    1.588817] musb-hdrc musb-hdrc.1.auto: Enabled SW babble control
    [    1.595567] musb-hdrc musb-hdrc.1.auto: Falied to request rx1.
    [    1.601758] musb-hdrc musb-hdrc.1.auto: musb_init_controller failed with status -517
    [    1.610058] platform musb-hdrc.1.auto: Driver musb-hdrc requests probe deferral
    [    1.641294] mousedev: PS/2 mouse device common for all mice
    [    1.653682] omap_rtc 44e3e000.rtc: rtc core: registered 44e3e000.rtc as rtc1
    [    1.663286] i2c /dev entries driver
    [    1.668000] Driver for 1-wire Dallas network protocol.
    [    1.677722] omap_wdt: OMAP Watchdog Timer Rev 0x01: initial timeout 60 sec
    [    1.688105] edma-dma-engine edma-dma-engine.0: allocated channel for 0:25
    [    1.695691] edma-dma-engine edma-dma-engine.0: allocated channel for 0:24
    [    1.743108] omap_hsmmc 47810000.mmc: unable to obtain RX DMA engine channel 3229886772
    [    1.762948] ledtrig-cpu: registered to indicate activity on CPUs
    [    1.769947] edma-dma-engine edma-dma-engine.0: allocated channel for 0:36
    [    1.777341] omap-sham 53100000.sham: hw accel on OMAP rev 4.3
    [    1.785822] omap-aes 53500000.aes: OMAP AES hw accel rev: 3.2
    [    1.791944] edma-dma-engine edma-dma-engine.0: allocated channel for 0:5
    [    1.799237] edma-dma-engine edma-dma-engine.0: allocated channel for 0:6
    [    1.808448] hidraw: raw HID events driver (C) Jiri Kosina
    [    1.815060] usbcore: registered new interface driver usbhid
    [    1.820946] usbhid: USB HID core driver
    [    1.830419] oprofile: using arm/armv7
    [    1.835082] TCP: cubic registered
    [    1.838635] Initializing XFRM netlink socket
    [    1.843308] NET: Registered protocol family 17
    [    1.848043] mmc0: host does not support reading read-only switch. assuming write-enable.
    [    1.856869] NET: Registered protocol family 15
    [    1.861563] can: controller area network core (rev 20120528 abi 9)
    [    1.868276] NET: Registered protocol family 29
    [    1.873147] mmc0: new high speed SD card at address e624
    [    1.879255] 8021q: 802.1Q VLAN Support v1.8
    [    1.884438] Key type dns_resolver registered
    [    1.889446] mmcblk0: mmc0:e624 SU02G 1.84 GiB 
    [    1.894930] cpu cpu0: cpu0 regulator not ready, retry
    [    1.900298] platform cpufreq-cpu0.0: Driver cpufreq-cpu0 requests probe deferral
    [    1.909995] ThumbEE CPU extension supported.
    [    1.914874]  mmcblk0: p1
    [    1.918939] Registering SWP/SWPB emulation handler
    [    1.926222] vcc1v8: disabling
    [    1.929376] regulator-dummy: disabling
    [    1.934710] Error: Driver 'tfp410' is already registered, aborting...
    [    1.945782] [drm] Supports vblank timestamp caching Rev 1 (10.10.2010).
    [    1.952926] [drm] No driver support for vblank timestamp query.
    [    2.011796] Console: switching to colour frame buffer device 100x37
    [    2.026748] tilcdc 4830e000.lcdc: fb0:  frame buffer device
    [    2.032657] tilcdc 4830e000.lcdc: registered panic notifier
    [    2.038573] [drm] Initialized tilcdc 1.0.0 20121205 on minor 0
    [    2.046005] omap-gpmc 50000000.gpmc: GPMC revision 6.0
    [    2.051448] gpmc_mem_init: disabling cs 0 mapped at 0x0-0x1000000
    [    2.059344] ONFI param page 0 valid
    [    2.063135] ONFI flash detected
    [    2.066463] NAND device: Manufacturer ID: 0x2c, Chip ID: 0xdc (Micron MT29F4G08ABADAH4), 512MiB, page size: 2048, OOB size: 64
    [    2.078510] omap2-nand: detected x8 NAND flash
    [    2.083223] nand: using OMAP_ECC_BCH8_CODE_HW ECC scheme
    [    2.088889] omap2-nand: using custom ecc layout
    [    2.093914] 9 ofpart partitions found on MTD device omap2-nand.0
    [    2.100254] Creating 9 MTD partitions on "omap2-nand.0":
    [    2.105930] 0x000000000000-0x000000020000 : "xload"
    [    2.113811] 0x000000020000-0x000000040000 : "xload_backup1"
    [    2.122145] 0x000000040000-0x000000060000 : "xload_backup2"
    [    2.130647] 0x000000060000-0x000000080000 : "xload_backup3"
    [    2.139193] 0x000000080000-0x000000100000 : "barebox"
    [    2.147353] 0x000000100000-0x000000140000 : "bareboxenv"
    [    2.155645] 0x000000140000-0x000000180000 : "oftree"
    [    2.163615] 0x000000180000-0x000000980000 : "kernel"
    [    2.178037] 0x000000980000-0x000020000000 : "root"
    [    2.609528] tps65910 0-002d: No interrupt support, no core IRQ
    [    2.621078] vrtc: 1800 mV 
    [    2.624614] vrtc: supplied by vcc5v
    [    2.630355] vio: at 1500 mV 
    [    2.633635] vio: supplied by vcc5v
    [    2.639406] vdd_mpu: 912 <--> 1375 mV at 1100 mV 
    [    2.644648] vdd_mpu: supplied by vcc5v
    [    2.650778] vdd_core: 912 <--> 1150 mV at 1100 mV 
    [    2.656039] vdd_core: supplied by vcc5v
    [    2.661862] vdd3: 5000 mV 
    [    2.666355] vdig1_1p8v: 1800 mV 
    [    2.669868] vdig1_1p8v: supplied by vcc5v
    [    2.675903] vdig2: at 1800 mV 
    [    2.679205] vdig2: supplied by vcc5v
    [    2.684763] vpll: at 1800 mV 
    [    2.688008] vpll: supplied by vcc5v
    [    2.693476] vdac: at 1800 mV 
    [    2.696708] vdac: supplied by vcc5v
    [    2.702068] vaux1: at 1800 mV 
    [    2.705498] vaux1: supplied by vcc5v
    [    2.711003] vaux2: at 3300 mV 
    [    2.714431] vaux2: supplied by vcc5v
    [    2.719894] vaux33: at 3300 mV 
    [    2.723472] vaux33: supplied by vcc5v
    [    2.729055] vmmc: 3300 mV 
    [    2.732014] vmmc: supplied by vcc5v
    [    2.737940] at24 0-0052: 4096 byte 24c32 EEPROM, writable, 32 bytes/write
    [    2.746084] at24 0-0051: 32768 byte 24c256 EEPROM, writable, 64 bytes/write
    [    2.754382] at24 0-0050: 32768 byte 24c256 EEPROM, writable, 64 bytes/write
    [    2.762809] rtc-m41t80 0-0068: chip found, driver version 0.05
    [    2.769962] rtc-m41t80 0-0068: rtc core: registered rv4162c7 as rtc0
    [    2.779105] edt_ft5x06 0-0038: touchscreen probe failed
    [    2.784869] edt_ft5x06: probe of 0-0038 failed with error -121
    [    2.791099] omap_i2c 44e0b000.i2c: bus 0 rev0.11 at 400 kHz
    [    2.797720] musb-hdrc musb-hdrc.0.auto: Enabled SW babble control
    [    2.806935] musb-hdrc musb-hdrc.0.auto: MUSB HDRC host driver
    [    2.813769] musb-hdrc musb-hdrc.0.auto: new USB bus registered, assigned bus number 1
    [    2.822395] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002
    [    2.829641] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
    [    2.837313] usb usb1: Product: MUSB HDRC host driver
    [    2.842583] usb usb1: Manufacturer: Linux 3.12.30-AM335x-PD15.2.1 musb-hcd
    [    2.849838] usb usb1: SerialNumber: musb-hdrc.0.auto
    [    2.856517] hub 1-0:1.0: USB hub found
    [    2.860583] hub 1-0:1.0: 1 port detected
    [    2.865916] musb-hdrc musb-hdrc.1.auto: Enabled SW babble control
    [    2.875033] musb-hdrc musb-hdrc.1.auto: MUSB HDRC host driver
    [    2.881750] musb-hdrc musb-hdrc.1.auto: new USB bus registered, assigned bus number 2
    [    2.890492] usb usb2: New USB device found, idVendor=1d6b, idProduct=0002
    [    2.897750] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
    [    2.905415] usb usb2: Product: MUSB HDRC host driver
    [    2.910657] usb usb2: Manufacturer: Linux 3.12.30-AM335x-PD15.2.1 musb-hcd
    [    2.917949] usb usb2: SerialNumber: musb-hdrc.1.auto
    [    2.924587] hub 2-0:1.0: USB hub found
    [    2.928650] hub 2-0:1.0: 1 port detected
    [    2.982527] tilcdc 4830e000.lcdc: timeout waiting for framedone
    [    3.023431] UBI: attaching mtd13 to ubi0
    [    3.332702] usb 2-1: new high-speed USB device number 2 using musb-hdrc
    [    3.492893] usb 2-1: New USB device found, idVendor=0424, idProduct=2514
    [    3.499975] usb 2-1: New USB device strings: Mfr=0, Product=0, SerialNumber=0
    [    3.533257] hub 2-1:1.0: USB hub found
    [    3.537392] hub 2-1:1.0: 4 ports detected
    [    4.256550] UBI: scanning is finished
    [    4.273048] UBI: attached mtd13 (name "root", size 502 MiB) to ubi0
    [    4.279709] UBI: PEB size: 131072 bytes (128 KiB), LEB size: 129024 bytes
    [    4.286960] UBI: min./max. I/O unit sizes: 2048/2048, sub-page size 512
    [    4.293988] UBI: VID header offset: 512 (aligned 512), data offset: 2048
    [    4.301083] UBI: good PEBs: 4020, bad PEBs: 0, corrupted PEBs: 0
    [    4.307460] UBI: user volume: 1, internal volumes: 1, max. volumes count: 128
    [    4.315033] UBI: max/mean erase counter: 1/0, WL threshold: 4096, image sequence number: 32625
    [    4.324169] UBI: available PEBs: 0, total reserved PEBs: 4020, PEBs reserved for bad PEB handling: 80
    [    4.333987] UBI: background thread "ubi_bgt0d" started, PID 998
    [    4.402526] davinci_mdio 4a101000.mdio: davinci mdio revision 1.6
    [    4.409001] davinci_mdio 4a101000.mdio: detected phy mask fffffffe
    [    4.416489] libphy: 4a101000.mdio: probed
    [    4.420751] davinci_mdio 4a101000.mdio: phy[0]: device 4a101000.mdio:00, driver SMSC LAN8710/LAN8720
    [    4.431437] Detected MACID = b0:d5:cc:cc:d9:4a
    [    4.439489] rtc-m41t80 0-0068: hctosys: unable to read the hardware clock
    [    4.468543] UBIFS: background thread "ubifs_bgt0_0" started, PID 1012
    [    4.489074] UBIFS: start fixing up free space
    [   13.029652] UBIFS: free space fixup complete
    [   13.047843] UBIFS: mounted UBI device 0, volume 0, name "root"
    [   13.054115] UBIFS: LEB size: 129024 bytes (126 KiB), min./max. I/O unit sizes: 2048 bytes/2048 bytes
    [   13.063818] UBIFS: FS size: 506419200 bytes (482 MiB, 3925 LEBs), journal size 9033728 bytes (8 MiB, 71 LEBs)
    [   13.074336] UBIFS: reserved for root: 0 bytes (0 KiB)
    [   13.079692] UBIFS: media format: w4/r0 (latest is w4/r0), UUID 79C65332-767A-4C8E-B815-9AC549B450F6, small LPT model
    [   13.092211] VFS: Mounted root (ubifs filesystem) on device 0:14.
    [   13.099976] devtmpfs: mounted
    [   13.103692] Freeing unused kernel memory: 396K (c07fd000 - c0860000)
    [   13.342416] NET: Registered protocol family 10
    [   13.351058] systemd[1]: Inserted module 'ipv6'
    [   13.393602] systemd[1]: systemd 219 running in system mode. (-PAM -AUDIT -SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP -LIBCRYPTSETUP -GCRYPT -GNUTLS +ACL +XZ -LZ4 -SECCOMP +BLKID -ELFUTILS +KMOD -IDN)
    [   13.413746] systemd[1]: Detected architecture arm.
    
    Welcome to The Yogurt Distribution AM335x-PD15.2.1!
    
    [   13.443799] systemd[1]: Set hostname to <phycore-am335x-1>.
    [   13.454778] systemd[1]: Initializing machine ID from random generator.
    [   13.634801] systemd-sysv-generator[1020]: Overwriting existing symlink /run/systemd/generator.late/pvr-init.service with real service
    [   13.909081] systemd[1]: Cannot add dependency job for unit org.freedesktop.resolve1.busname, ignoring: Unit org.freedesktop.resolve1.busname failed to load: No such file or directory.
    [   13.935091] systemd[1]: Started Forward Password Requests to Wall Directory Watch.
    [   13.943398] systemd[1]: Starting Forward Password Requests to Wall Directory Watch.
    [   13.952026] systemd[1]: Started Dispatch Password Requests to Console Directory Watch.
    [   13.960673] systemd[1]: Starting Dispatch Password Requests to Console Directory Watch.
    [  OK  ] Created slice Root Slice.
    [   13.992732] systemd[1]: Created slice Root Slice.
    [   13.997850] systemd[1]: Starting Root Slice.
    [  OK  ] Created slice User and Session Slice.
    [   14.022697] systemd[1]: Created slice User and Session Slice.
    [   14.028895] systemd[1]: Starting User and Session Slice.
    [  OK  ] Listening on networkd rtnetlink socket.
    [   14.053018] systemd[1]: Listening on networkd rtnetlink socket.
    [   14.059422] systemd[1]: Starting networkd rtnetlink socket.
    [  OK  ] Listening on Journal Socket (/dev/log).
    [   14.082724] systemd[1]: Listening on Journal Socket (/dev/log).
    [   14.089096] systemd[1]: Starting Journal Socket (/dev/log).
    [  OK  ] Created slice System Slice.
    [   14.112716] systemd[1]: Created slice System Slice.
    [   14.118022] systemd[1]: Starting System Slice.
    [  OK  ] Created slice system-serial\x2dgetty.slice.
    [   14.142716] systemd[1]: Created slice system-serial\x2dgetty.slice.
    [   14.149453] systemd[1]: Starting system-serial\x2dgetty.slice.
    [  OK  ] Reached target Slices.
    [   14.172702] systemd[1]: Reached target Slices.
    [   14.177506] systemd[1]: Starting Slices.
    [  OK  ] Listening on Delayed Shutdown Socket.
    [   14.202705] systemd[1]: Listening on Delayed Shutdown Socket.
    [   14.208889] systemd[1]: Starting Delayed Shutdown Socket.
    [  OK  ] Listening on Journal Socket.
    [   14.232707] systemd[1]: Listening on Journal Socket.
    [   14.238093] systemd[1]: Starting Journal Socket.
    [   14.246280] systemd[1]: Starting Setup Virtual Console...
             Starting Setup Virtual Console...
    [   14.286488] systemd[1]: Mounting Temporary Directory...
             Mounting Temporary Directory...
    [   14.333681] systemd[1]: Mounted Huge Pages File System.
    [   14.354570] systemd[1]: Started Load Kernel Modules.
    [   14.367687] systemd[1]: Starting Create list of required static device nodes for the current kernel...
             Starting Create list of required st... nodes for the current kernel...
    [   14.425534] systemd[1]: Mounting POSIX Message Queue File System...
             Mounting POSIX Message Queue File System...
    [   14.465978] systemd[1]: Mounting Debug File System...
             Mounting Debug File System...
    [   14.483377] systemd[1]: Started File System Check on Root Device.
    [   14.508952] systemd[1]: Starting Remount Root and Kernel File Systems...
             Starting Remount Root and Kernel File Systems...
    [  OK  ] Listening on udev Control Socket.
    [   14.563119] systemd[1]: Listening on udev Control Socket.
    [   14.569047] systemd[1]: Starting udev Control Socket.
    [  OK  ] Reached target Remote File Systems.
    [   14.612888] systemd[1]: Reached target Remote File Systems.
    [   14.618957] systemd[1]: Starting Remote File Systems.
    [  OK  ] Listening on udev Kernel Socket.
    [   14.642787] systemd[1]: Listening on udev Kernel Socket.
    [   14.648546] systemd[1]: Starting udev Kernel Socket.
    [   14.669258] systemd[1]: Listening on Journal Audit Socket.
    [   14.678102] systemd[1]: Starting Journal Service...
             Starting Journal Service...
    [  OK  ] Reached target Paths.
    [   14.733274] systemd[1]: Reached target Paths.
    [   14.738064] systemd[1]: Starting Paths.
    [  OK  ] Created slice system-getty.slice.
    [   14.783310] systemd[1]: Created slice system-getty.slice.
    [   14.789184] systemd[1]: Starting system-getty.slice.
    [  OK  ] Listening on /dev/initctl Compatibility Named Pipe.
    [   14.832931] systemd[1]: Listening on /dev/initctl Compatibility Named Pipe.
    [   14.840461] systemd[1]: Starting /dev/initctl Compatibility Named Pipe.
    [   14.863350] systemd[1]: Mounted Configuration File System.
    [   14.871826] systemd[1]: Starting udev Coldplug all Devices...
             Starting udev Coldplug all Devices...
    [  OK  ] Reached target Swap.
    [   14.942922] systemd[1]: Reached target Swap.
    [   14.947629] systemd[1]: Starting Swap.
    [   14.974561] systemd[1]: Starting Apply Kernel Variables...
             Starting Apply Kernel Variables...
    [   15.033109] systemd[1]: Mounted FUSE Control File System.
    [  OK  ] Mounted Debug File System.
    [   15.063054] systemd[1]: Mounted Debug File System.
    [  OK  ] Mounted POSIX Message Queue File System.
    [   15.083097] systemd[1]: Mounted POSIX Message Queue File System.
    [  OK  ] Mounted Temporary Directory.
    [   15.102924] systemd[1]: Mounted Temporary Directory.
    [  OK  ] Started Journal Service.
    [   15.133093] systemd[1]: Started Journal Service.
    [  OK  ] Started Setup Virtual Console.
    [  OK  ] Started Create list of required sta...ce nodes for the current kernel.
    [  OK  ] Started Remount Root and Kernel File Systems.
    [  OK  ] Started Apply Kernel Variables.
             Starting Rebuild Hardware Database...
             Starting Create System Users...
             Starting Flush Journal to Persistent Storage...
    [   15.667190] systemd-journald[1042]: Received request to flush runtime journal from PID 1
    [  OK  ] Started Flush Journal to Persistent Storage.
    [  OK  ] Started Create System Users.
             Starting Create Static Device Nodes in /dev...
    [  OK  ] Started Create Static Device Nodes in /dev.
    [  OK  ] Reached target Local File Systems (Pre).
             Mounting /var/volatile...
    [  OK  ] Mounted /var/volatile.
    [  OK  ] Reached target Local File Systems.
             Starting Rebuild Journal Catalog...
             Starting Create Volatile Files and Directories...
             Starting Load/Save Random Seed...
    [  OK  ] Started Load/Save Random Seed.
    [  OK  ] Started Rebuild Journal Catalog.
    [  OK  ] Started Create Volatile Files and Directories.
             Starting Network Time Synchronization...
             Starting Run pending postinsts...
             Starting Update UTMP about System Boot/Shutdown...
    [  OK  ] Started Network Time Synchronization.
    [  OK  ] Reached target System Time Synchronized.
    [  OK  ] Started Update UTMP about System Boot/Shutdown.
    [  OK  ] Started udev Coldplug all Devices.
    [  OK  ] Started Rebuild Hardware Database.
             Starting Update is Completed...
             Starting udev Kernel Device Manager...
    [  OK  ] Started Update is Completed.
    [  OK  ] Started udev Kernel Device Manager.
    [   21.232817] PM: request_firmware failed
    [  OK  ] Created slice system-systemd\x2dbacklight.slice.
             Starting Load/Save Screen Backlight...ess of backlight:backlight.10...
    [  OK  ] Found device /dev/ttyO0.
    [  OK  ] Started Load/Save Screen Backlight ...tness of backlight:backlight.10.
    [   22.473000] wm8974 0-001a: Failed to issue reset
    [   22.477922] wm8974 0-001a: ASoC: failed to probe CODEC -121
    [   22.538232] davinci_phycore sound_iface.8: ASoC: failed to instantiate card -121
    [   22.625331] davinci_phycore sound_iface.8: snd_soc_register_card failed (-121)
    [   22.719562] davinci_phycore: probe of sound_iface.8 failed with error -121
    [   23.940598] systemd-sysv-generator[1964]: Overwriting existing symlink /run/systemd/generator.late/pvr-init.service with real service
    [   24.386531] systemd-sysv-generator[1980]: Overwriting existing symlink /run/systemd/generator.late/pvr-init.service with real service
    [  OK  ] Started Run pending postinsts.
    [  OK  ] Reached target System Initialization.
    [  OK  ] Listening on RPCbind Server Activation Socket.
    [  OK  ] Listening on D-Bus System Message Bus Socket.
    [  OK  ] Reached target Timers.
             Starting Restore Sound Card State...
    [  OK  ] Listening on sshd.socket.
    [  OK  ] Reached target Sockets.
    [  OK  ] Reached target Basic System.
             Starting Login Service...
    [  OK  ] Started can0 interface setup.
             Starting can0 interface setup...
    [  OK  ] Started D-Bus System Message Bus.
    [   24.985102] c_can_platform 481cc000.d_can can0: setting BTR=5c4e BRPE=0000
             Starting D-Bus System Message Bus...
             Starting Network Service...
             Starting SYSV: powervr module loader...
             Starting Permit User Sessions...
    [  OK  ] Started Restore Sound Card State.
    [  OK  ] Started Permit User Sessions.
    [  OK  ] Started Login Service.
    [  OK  ] Started Network Service.
    [   25.608350] net eth0: initializing cpsw version 1.12 (0)
    [  OK  ] Reached target Network.
    [   25.636031] net eth0: phy found : id is : 0x7c0f1
             Starting Target Communication Framework agent...
    [   25.689363] 8021q: adding VLAN 0 to HW filter on device eth0
             Starting Network Time Service...
             Starting Network Name Resolution...
    [  OK  ] Started Getty on tty1.
    [   25.753680] IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
             Starting Getty on tty1...
    [  OK  ] Started Serial Getty on ttyO0.
             Starting Serial Getty on ttyO0...
    [  OK  ] Reached target Login Prompts.
    [  OK  ] Started SYSV: powervr module loader.
             Starting Autostart Qt 5 Demo...
    [  OK  ] Started Autostart Qt 5 Demo.
    [  OK  ] Started Target Communication Framework agent.
    [  OK  ] Started Network Name Resolution.
    [  OK  ] Started Network Time Service.
    [  OK  ] Reached target Multi-User System.
             Starting Update UTMP about System Runlevel Changes...
    [  OK  ] Started Update UTMP about System Runlevel Changes.
    
     ____   _   _ __   __ _____  _____   ____
    |  _ \ | | | |\ \ / /|_   _|| ____| / ___|
    | |_) || |_| | \ V /   | |  |  _|  | |
    |  __/ |  _  |  | |    | |  | |___ | |___
    |_|    |_| |_|  |_|    |_|  |_____| \____|
    
    __   __  ___    ____  _   _  ____   _____
    \ \ / / / _ \  / ___|| | | ||  _ \ |_   _|
     \ V / | | | || |  _ | | | || |_) |  | |
      | |  | |_| || |_| || |_| ||  _ <   | |
      |_|   \___/  \____| \___/ |_| \_\  |_|
    
    
    Yogurt (Phytec Example Distribution) AM335x-PD15.2.1 phycore-am335x-1 ttyO0
    
    phycore-am335x-1 login: [   28.653206] libphy: 4a101000.mdio:00 - Link is Up - 100/Full
    [   28.673145] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
    
     ____   _   _ __   __ _____  _____   ____
    |  _ \ | | | |\ \ / /|_   _|| ____| / ___|
    | |_) || |_| | \ V /   | |  |  _|  | |
    |  __/ |  _  |  | |    | |  | |___ | |___
    |_|    |_| |_|  |_|    |_|  |_____| \____|
    
    __   __  ___    ____  _   _  ____   _____
    \ \ / / / _ \  / ___|| | | ||  _ \ |_   _|
     \ V / | | | || |  _ | | | || |_) |  | |
      | |  | |_| || |_| || |_| ||  _ <   | |
      |_|   \___/  \____| \___/ |_| \_\  |_|
    
    
    Yogurt (Phytec Example Distribution) AM335x-PD15.2.1 phycore-am335x-1 ttyO0
    
    phycore-am335x-1 login: 
    ```

!!! tip 
    Appuyer sur la touche **entrée** pour faire <ins>apparaître le prompt</ins> correctement.

02 - Création placeholders
--------------------------


```sh
# Se loguer en tant que root
mkdir -p /home/{images,.ssh}
```

03 - IP module PHYTEC
---------------------

Récupérer l'ip du futur distributeur:

```sh
ifconfig eth0 | grep Mask | awk '{print $2}' | cut -d ':' -f 2
```

!!! example "Exemple résultat sortie standard"
    192.168.0.16

04 - Envoi fichiers sur module PHYTEC
-------------------------------------

Dans la VM **éditer le bashrc** et **remplacer** toutes les **adresses IP** du fichier par l'adresse IP du module PHYTEC.


```sh
# vim ~/.bashrc

:% s/192.168.0.15/192.168.0.<ip>/gc
```

Faire un **source** du <ins>~/.bashrc</ins>

05 - Connection distributeur
----------------------------

Depuis un **nouveau terminal**, faire un **lockto <ip\> du module PHYTEC** pour établir la connection ssh et ajouter le mot de passe demandé depuis le prompt au besoin.

??? info "Resultat sortie standard"
    ```sh
    ~/b/m/script ❯❯❯ v ~/.bashrc
    ~/b/m/script ❯❯❯ bash
    Taper gethelp pour obtenir de l'aide sur les commandes disponibles
    phyvm@so530v4:~/b2c/my-own/script$ lockto 18
    The authenticity of host '192.168.0.18 (192.168.0.18)' can't be established.
    ECDSA key fingerprint is 16:18:78:c4:17:50:02:01:43:07:71:d8:8a:a4:8c:c7.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added '192.168.0.18' (ECDSA) to the list of known hosts.
    Last login: Thu Aug 30 12:43:01 2018
     ____   _   _ __   __ _____  _____   ____
    |  _ \ | | | |\ \ / /|_   _|| ____| / ___|
    | |_) || |_| | \ V /   | |  |  _|  | |
    |  __/ |  _  |  | |    | |  | |___ | |___
    |_|    |_| |_|  |_|    |_|  |_____| \____|
    
    __   __  ___    ____  _   _  ____   _____
    \ \ / / / _ \  / ___|| | | ||  _ \ |_   _|
     \ V / | | | || |  _ | | | || |_) |  | |
      | |  | |_| || |_| || |_| ||  _ <   | |
      |_|   \___/  \____| \___/ |_| \_\  |_|
    
    
    Yogurt (Phytec Example Distribution) AM335x-PD15.2.1 
    
    root@phycore-am335x-1:~# 
    ```

!!! info
    Si le **lockto** refuse de fonctionner faire la commande suivante.

    ```sh
    ssh-keygen -f "/home/phyvm/.ssh/known_hosts" -R <address-ip-module-phytec>
    ```

    !!! example "Exemple avec ip du distributeur fabrication"
        ssh-keygen -f "/home/phyvm/.ssh/known_hosts" -R 192.168.0.16

Puis **se déconnecter** du module <ins>PHYTEC</ins>.

06 - Déploiement
----------------

??? info "Problême de connection ssh"
    Si **problème de connection ssh** avec la cible (de type man on the middle) alors taper la commande affichée à l'ecran du terminal.

Récupérer les fichiers systèmes et programmes avec les aliases du bashrc:

!!! attention
    Si ce n'est pas deja le cas, faire un **source du ~/.bashrc** une fois les nouvelles adresses ip modifiées pour que les changements soient pris en compte.

1. getsystem
2. getdistrib
3. getimages

??? info "Résultat sortie standard"
    ```sh
    phyvm@so530v4:~/b2c/my-own/script$ source ~/.bashrc
    Taper gethelp pour obtenir de l'aide sur les commandes disponibles
    phyvm@so530v4:~/b2c/my-own/script$ getsystem
    distrib_start.sh                                                     100% 1298     1.3KB/s   00:00    
    distrib_fct.sh                                                       100%  531     0.5KB/s   00:00    
    config.txt                                                           100%   55     0.1KB/s   00:00    
    new2dis.service                                                      100%  138     0.1KB/s   00:00    
    distrib_fct.service                                                  100%  202     0.2KB/s   00:00    
    libQt5SerialPort.so.5                                                100%   83KB  82.7KB/s   00:00    
    eGTouchD                                                             100%  178KB 177.9KB/s   00:00    
    eGTouchL.ini                                                         100% 3277     3.2KB/s   00:00    
    id_rsa_srv_ow                                                        100% 3243     3.2KB/s   00:00    
    config                                                               100%   68     0.1KB/s   00:00    
    phyvm@so530v4:~/b2c/my-own/script$ getdistrib
    DISTRIBUTEUR                                                         100%   17MB  17.4MB/s   00:01    
    default_maint.qss                                                    100% 7703     7.5KB/s   00:00    
    default.qss                                                          100%   10KB   9.9KB/s   00:00    
    phyvm@so530v4:~/b2c/my-own/script$ getimages
    billet_5_10_20_50.png                                                100%   73KB  73.2KB/s   00:00    
    billets.png                                                          100%  145KB 145.2KB/s   00:00    
    boouton_retour.png                                                   100%  651     0.6KB/s   00:00    
    bouton_assistance.png                                                100%  973     1.0KB/s   00:00    
    bouton_gris_petit.png                                                100% 2871     2.8KB/s   00:00    
    bouton_gris.png                                                      100% 2704     2.6KB/s   00:00    
    bouton_jaune.png                                                     100% 2773     2.7KB/s   00:00    
    bouton_ok.png                                                        100% 1443     1.4KB/s   00:00    
    bouton_ok_prev.png                                                   100% 1366     1.3KB/s   00:00    
    bouton_ok.xcf                                                        100% 2737     2.7KB/s   00:00    
    bouton.png                                                           100% 1077     1.1KB/s   00:00    
    bouton_sys2.png                                                      100%  600     0.6KB/s   00:00    
    bouton_sys.png                                                       100%  922     0.9KB/s   00:00    
    carte-bleue.jpeg                                                     100%   29KB  29.3KB/s   00:00    
    carte_opticard_invalide.png                                          100%   18KB  18.4KB/s   00:00    
    centerpay.png                                                        100% 3670     3.6KB/s   00:00    
    chargement_opticard.png                                              100%   24KB  23.7KB/s   00:00    
    distrib_cartes.png                                                   100%  208KB 207.6KB/s   00:00    
    distrib_hs.png                                                       100% 9994     9.8KB/s   00:00    
    distrib_jetons.png                                                   100%  183KB 182.6KB/s   00:00    
    distribution_jetons2.png                                             100%   98KB  97.9KB/s   00:00    
    distribution_jetons.png                                              100%   21KB  21.0KB/s   00:00    
    fleches_insertion_carte.png                                          100% 1921     1.9KB/s   00:00    
    fond_btn.png                                                         100% 1317     1.3KB/s   00:00    
    fond_justif_ticket.png                                               100% 1153     1.1KB/s   00:00    
    fond.png                                                             100%   78KB  77.7KB/s   00:00    
    fond_x1.png                                                          100%   41KB  41.3KB/s   00:00    
    fond_x2.png                                                          100%   41KB  41.1KB/s   00:00    
    fond_x3.png                                                          100%   39KB  39.3KB/s   00:00    
    fond_x4.png                                                          100%   45KB  44.8KB/s   00:00    
    fond_xx.png                                                          100%  211KB 211.1KB/s   00:00    
    imprimante.png                                                       100%  147KB 147.4KB/s   00:00    
    init_en_cour.png                                                     100% 8623     8.4KB/s   00:00    
    inserer1.png                                                         100% 1472     1.4KB/s   00:00    
    inserer2.png                                                         100% 1421     1.4KB/s   00:00    
    inserer3.png                                                         100% 1797     1.8KB/s   00:00    
    inserer_billet.xcf                                                   100%   33KB  33.0KB/s   00:00    
    inserer_carte_opticard3.png                                          100%   14KB  13.9KB/s   00:00    
    inserer_carte_opticard.png                                           100%   17KB  16.6KB/s   00:00    
    inserer_CB2.png                                                      100% 6286     6.1KB/s   00:00    
    inserer_CB3.png                                                      100% 9527     9.3KB/s   00:00    
    insert_billet.png                                                    100%   55KB  54.9KB/s   00:00    
    insert_cb.png                                                        100%   90KB  90.1KB/s   00:00    
    insertion_billet2.png                                                100%   16KB  15.9KB/s   00:00    
    insertion_billet.png                                                 100%   18KB  18.5KB/s   00:00    
    insertion_CB.png                                                     100%   12KB  12.0KB/s   00:00    
    jetons_icone.png                                                     100%   14KB  14.0KB/s   00:00    
    jetons.png                                                           100%   26KB  26.4KB/s   00:00    
    lecteur_billet.png                                                   100%  216KB 215.8KB/s   00:00    
    lecteur_cb2.png                                                      100%   92KB  91.7KB/s   00:00    
    logo_cb.png                                                          100% 4570     4.5KB/s   00:00    
    logo_heurtaux.png                                                    100% 4227     4.1KB/s   00:00    
    logo_opticard.png                                                    100% 5426     5.3KB/s   00:00    
    logo_opticard_prev.png                                               100% 4022     3.9KB/s   00:00    
    main_jetons.png                                                      100%   14KB  13.9KB/s   00:00    
    mousseauto_anim3.gif                                                 100%  242KB 242.4KB/s   00:00    
    mousseauto_anim.gif                                                  100%  109KB 109.0KB/s   00:00    
    mousseauto_anim.xcf                                                  100%  215KB 214.9KB/s   00:00    
    myanim.gif                                                           100%   92KB  92.2KB/s   00:00    
    old_PRESENTATION_DISTRIBUTEUR.gif                                    100% 1689KB   1.7MB/s   00:01    
    opticard2_icone.png                                                  100%   22KB  22.2KB/s   00:00    
    path6708.png                                                         100%   21KB  21.0KB/s   00:00    
    personnage_mousseauto.png                                            100%   72KB  71.6KB/s   00:00    
    prenez_carte.png                                                     100%   35KB  35.4KB/s   00:00    
    prenez_jetons.png                                                    100%   85KB  85.5KB/s   00:00    
    prenez_justif.png                                                    100%   30KB  29.6KB/s   00:00    
    PRESENTATION_DISTRIBUTEUR.gif                                        100% 1641KB   1.6MB/s   00:00    
    prevmousseauto_anim.xcf                                              100%  215KB 215.0KB/s   00:00    
    retirer1.png                                                         100% 2162     2.1KB/s   00:00    
    retirer2.png                                                         100% 1988     1.9KB/s   00:00    
    retirer3.png                                                         100% 2386     2.3KB/s   00:00    
    retirer_opticard3.png                                                100%   27KB  26.9KB/s   00:00    
    retrait_opticard.png                                                 100%   31KB  30.7KB/s   00:00    
    roll_up.gif                                                          100%   16MB   7.8MB/s   00:02    
    transac_en_cour.png                                                  100%   18KB  18.1KB/s   00:00    
    TR_FR.png                                                            100% 8369     8.2KB/s   00:00    
    TR_GB.png                                                            100%   10KB  10.5KB/s   00:00    
    TR_GER.png                                                           100% 8151     8.0KB/s   00:00    
    TR_IT.png                                                            100% 8343     8.2KB/s   00:00    
    TR_SP.png                                                            100% 8350     8.2KB/s   00:00    
    Z18.JPG                                                              100%  205KB 205.4KB/s   00:00    
    z18.png                                                              100%   92KB  91.9KB/s   00:00    
    phyvm@so530v4:~/b2c/my-own/script$ 
    ```

07 - Services et scripts
------------------------

Depuis le <ins>terminal</ins> de **Microcom_ttyUSB0**.

Désactiver le service de démonstration proposé par PHYTEC:

```sh
# Killer le PID de qtDemo puis

systemctl disable phytec-qtdemo.service
```

Ajouter les services crées pour le distributeur:

```sh
systemctl enable new2dis.service
systemctl enable distrib_fct.service
```

Ajouter les droits sur les fichiers:

```sh
chmod a+x /home/distrib_start.sh 
chmod a+x /home/distrib_fct.sh
```

08 - Paramétrer le reverse ssh
------------------------------

Éditer le fichier **distrib_fct.sh** et remplacer les **ports reverse** ssh d'exemples pas les nouveaux.

```sh
vi /home/distrib_fct.sh
```
!!! note
    Renseigner les nouveaux ports reverse ssh dans le fichier **distrib3.odt**, onglet <ins>CMD</ins> qui se trouve à l'adresse:

```sh
/home/phytec/Desktop/HOME_BOULOT3/GESTION_DE_PROJET/distrib3.odt
```

09 - Reboot
-----------

Faire un reboot et s'assurer que tout est affiché correctement à l'écran.
