Préparation
===========

00 - BSP PTXDIST -> BSP PTX15
-----------------------------

!!! attention
    On part du **BSP PTXDIST** pour le passer en **BSP PD15**.

??? info "Carte SD déjà montée"
    Lorsqu'on part du **barebox personnalisé** par Christophe, si une **carte SD** est présente, elle est **automatiquement montée**.

```sh
# Commandes pour monter la carte SD, normalement, elle est déjà montée.
mci0.probe=1
mkdir -p /mnt/disk
mount /dev/mci0.0 /mnt/disk
```
___

00 - BSP PD15 (depuis)
----------------------

!!! attention
    Depuis un **BSP PD15**, jusqu'à présent on n'a pas encore utilisé cette étape.

```sh
# Si on part du BSP 15
mmc0.probe=1
mkdir -p /mnt/disk
mount /dev/mmc0.0 /mnt/disk
```
___

01 - Fichiers sur SD Card
-------------------------

??? info "Carte SD slot 1"
    La **carte SD** avec le contenu nécéssaire à la réalisation d'un module <ins>PHYTEC</ins> doit être présente dans le <ins>slot 1</ins> étiqueté **SD 1** de la carte designée par Christophe.

On liste l'**ensemble des fichiers sur la carte SD** pour s'assurer que tout le contenu souhaité est présent.

```sh
cd /mnt/disk/updates
ls -l
```

!!! info "Résultat sortie standard"
    ```sh
    barebox@Phytec phyCORE-AM335x:/mnt/disk/updates ls -l
    -rwxrwxrwx     370478 barebox.bin
    -rwxrwxrwx      43686 dist_dtb.0.ow
    -rwxrwxrwx    6572344 dist_kernel.0.ow
    -rwxrwxrwx  139216896 dist_rootfs.0.ow
    -rwxrwxrwx     101757 MLO
    -rwxrwxrwx       2219 na_yocto_sysupdate.sh
    -rwxrwxrwx     435448 oldbarebox.bin
    -rwxrwxrwx  139216896 root.ubifs
    -rwxrwxrwx    6572344 zImage.bin
    -rwxrwxrwx      43686 zImage.dtb
    ```

!!! danger
    Vérifier que les **fichiers** sont **présents** sur la carte SD avant le Flashage.
    Si on supprime **barebox.bin** et qu'il n'est pas présent sur la carte SD, **la carte est morte**, on ne pourra plus booter au prochain démarrage.

```sh
cd /mnt/disk
ls -l
```

!!! info "Résultat sortie standard"
    ```sh
    barebox@Phytec phyCORE-AM335x:/mnt/disk ls -l
    -rwxrwxrwx        944 maj_barebox
    -rwxrwxrwx     350052 barebox-image
    drwxrwxrwx          0 updates
    -rwxrwxrwx      15340 barebox-default-environment
    drwxrwxrwx          0 lastversion
    drwxrwxrwx          0 curversion
    drwxrwxrwx          0 config
    drwxrwxrwx          0 images_salon
    -rwxrwxrwx     370478 barebox.bin # doit être présent
    -rwxrwxrwx     101757 MLO
    ```
___

02 - Flashage MLO
-----------------

1. On supprime l'ancien **préboot loader** livré par <ins>PHYTEC</ins>.

2. On le remplace par le **MLO** personalisé.

```sh
# barebox@Phytec phyCORE-AM335x:/mnt/disk
erase   /dev/nand0.xload.bb
cp MLO  /dev/nand0.xload.bb
```
___

03 - Flashage Barebox
---------------------

1. On supprime l'ancien **barebox.bb** livré par <ins>PHYTEC</ins>.

2. On le remplace par le **barebox.bb** personalisé.

3. On efface les **variables d'environnement** pour **barebox.bb** pour le PTXdist qui ne sont plus utiles.

```sh
# barebox@Phytec phyCORE-AM335x:/mnt/disk

# Suppression et remplacement de barebox.bb.
erase /dev/nand0.barebox.bb
cp barebox.bin /dev/nand0.barebox.bb

# Effacement variable environnement.
erase /dev/nand0.bareboxenv.bb

# Si flashage depuis PTXDIST faire un reset
reset
```

!!! danger
    Suite au redémarrage, l'**autoboot** de **barebox** se lance, il faut l'**interrompre** avant la fin du **décompte** en appuyant sur la touche **Entrée**,

    sinon attendre un nouveau rédemarrage puis interrompre le décompte.

    Si l'on passe cette étape lorsque le **décompte tombe à zéro** le module <ins>PHYTEC</ins> redémarre peu importe ce que l'on est en train de faire.

??? info "Résultat sortie standard"
    ```sh
    barebox@Phytec phyCORE-AM335x:/mnt/disk reset
    
    
    barebox 2015.09.0-AM335x-PD15.2.1 #1 Thu Jan 14 19:05:05 CET 2016
    
    
    Board: Phytec phyCORE AM335x
    nand: ONFI flash detected
    nand: NAND device: Manufacturer ID: 0x2c, Chip ID: 0xdc (Micron MT29F4G08ABADAH4), 512MiB, page size: 2048, OOB size: 64
    booting from NAND
    
    
    barebox 2015.09.0-AM335x-PD15.2.1 #1 Fri Dec 22 14:31:26 CET 2017
    
    
    Board: Phytec phyCORE AM335x
    cpsw 4a100000.ethernet: detected phy mask 0x1
    mdio_bus: miibus0: probed
    eth0: got preset MAC address: b0:d5:cc:cc:d9:4a
    am335x-phy-driver 47401b00.usb-phy: am_usbphy 8ff20a24 enabled
    musb-hdrc: ConfigData=0xde (UTMI-8, dyn FIFOs, bulk combine, bulk split, HB-ISO Rx, HB-ISO Tx, SoftConn)
    musb-hdrc: MHDRC RTL version 2.0 
    musb-hdrc: setup fifo_mode 4
    musb-hdrc: 28/31 max ep, 16384/16384 memory
    m25p80 m25p80@00: s25sl064p (8192 Kbytes)
    i2c-omap 44e0b000.i2c: bus 0 rev0.11 at 400 kHz
    omap-hsmmc 48060000.mmc: registered as 48060000.mmc
    mmc0: detected SD card version 2.0
    mmc0: registered mmc0
    OMAP Watchdog Timer Rev 0x01
    nand: ONFI flash detected
    nand: NAND device: Manufacturer ID: 0x2c, Chip ID: 0xdc (Micron MT29F4G08ABADAH4), 512MiB, page size: 2048, OOB size: 64
    netconsole: registered as netconsole-1
    malloc space: 0x8ff00000 -> 0x9fdfffff (size 255 MiB)
    barebox-environment environment-nand.4: setting default environment path to /dev/nand0.bareboxenv.bb
    envfs: wrong magic
    running /env/bin/init...
    ******************************************************
    ** Distributeur   --- CENTERPAY ---                 **
    ******************************************************
    
    Stopper autoboot empeche auto update du kernel et rootfs
    
    Hit m for menu or any other key to stop autoboot:  2
    
    type exit to get to the menu
    barebox@Phytec phyCORE AM335x:/ 
    ```
    
Le <ins>décompte à stopper</ins> ici:
```sh
******************************************************
** Distributeur   --- CENTERPAY ---                 **
******************************************************

Stopper autoboot empeche auto update du kernel et rootfs

Hit m for menu or any other key to stop autoboot:  2

type exit to get to the menu
barebox@Phytec phyCORE AM335x:/ 
```
___

04 - Flashage Noyau et DTB
--------------------------

??? info "BSP PD15 préfix mmc0"
    A cette étape on part forcément du **BSP PD15** donc le préfix de probe est **mmc0**.

```sh
# On remonte la carte SD suite au redémarrage.
mmc0.probe=1    
mkdir -p /mnt/disk
mount /dev/mmc0.0 /mnt/disk

cd /mnt/disk/images_salon
```

On s'assure que les fichiers soient bien montés.

??? info "Résultat sortie standard"
    ```sh
    barebox@Phytec phyCORE AM335x:/mnt/disk/images_salon ls -l
    -rwxrwxrwx     101757 MLO
    -rwxrwxrwx     370495 barebox-phycore-am335x-1-2015.09.0-phy4-r6.0-20160326053918.bin
    -rwxrwxrwx     370478 barebox-phycore-am335x-1-2015.09.0-phy4-r6.0-20171222133051.bin
    -rwxrwxrwx     370478 barebox.bin
    -rwxrwxrwx  189536256 distrib-salon-phycore-am335x-1-20170928074938.rootfs.ubifs
    -rwxrwxrwx     370478 images_salon
    -rwxrwxrwx     370478 oldbarebox.bin
    -rwxrwxrwx  189536256 root.ubifs
    -rwxrwxrwx    4473712 zImage
    -rwxrwxrwx      42422 zImage-linux-ti-3.12.30-phy9-r0.0-am335x-phycore-rdk-20160325130625.dtb
    -rwxrwxrwx    4473712 zImage-linux-ti-3.12.30-phy9-r0.0-phycore-am335x-1-20160325130625.bin
    -rwxrwxrwx    4473712 zImage.bin
    -rwxrwxrwx      42422 zImage.dtb
    barebox@Phytec phyCORE AM335x:/mnt/disk/images_salon
    ```

On efface le **kernel** et on le <ins>remplace</ins> par le personnalisé.

```sh
# /mnt/disk/images_salon

erase /dev/nand0.kernel.bb
cp zImage.bin /dev/nand0.kernel.bb

erase /dev/nand0.oftree.bb
cp zImage.dtb /dev/nand0.oftree.bb
```
___

05 - Flashage rootfs
--------------------

```sh
# Une barre de progression s'affiche pour informer de l'état d'avancement du processus.
ubiformat     /dev/nand0.root 

ubiattach     /dev/nand0.root 
ubimkvol      /dev/nand0.root.ubi root 0

# On patient jusqu'au retour du prompt à la suite de cette commande.
cp root.ubifs /dev/nand0.root.ubi.root
```
___

XX - BSP PD17
-------------

!!! danger
    Procédure notée pour le passage en **BSP PD17** mais **jamais testée**!

```sh
mmc0.probe=1 
mkdir /mnt/disk
mount /dev/mmc0.0 /mnt/disk

(LINUX UBUNTU : mount -o noatime /dev/mmcblk0p1 /media/sdcard )


edit /env/network/eth0

barebox_update -t MLO.nand /mnt/tftp/MLO

barebox_update -t nand /mnt/tftp/barebox.bin

barebox_update -t nand /mnt/disk/barebox.bin

erase /dev/nand0.bareboxenv.bb

reset

ubiformat /dev/nand0.root
ubiattach /dev/nand0.root
ubimkvol -t static /dev/nand0.root.ubi kernel 8M
ubimkvol -t static /dev/nand0.root.ubi oftree 1M
ubimkvol -t dynamic /dev/nand0.root.ubi root 0

ubiupdatevol /dev/nand0.root.ubi.kernel /mnt/tftp/zImage.bin
ubiupdatevol /dev/nand0.root.ubi.oftree /mnt/tftp/zImage.dtb

cp -v /mnt/tftp/root.ubifs /dev/nand0.root.ubi.root  
```
