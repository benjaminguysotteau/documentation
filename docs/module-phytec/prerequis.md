Prérequis
=========

Synopsis
--------

<ins>PHYTEC</ins> envoie une carte avec une image de démonstration des capacités du module et une version de son **BSP** en **PTXDIST**.

La réalisation d'un module <ins>PHYTEC</ins> comprend les éléments suivants:

1. **MLO**
2. **Barebox**
3. **Noyau linux**
4. [DTB](terminologie.md#device-tree-blod-dtb)
5. **rootfs**
___

1. Le **MLO** est le pré-boot loader qui se charge du démarrage du module <ins>PHYTEC</ins>.
2. Le **Barebox** est l'applicatif <ins>PHYTEC</ins> pour manipuler le module (prompt, etc ...).
3. Le **noyau linux** est spécifique au module <ins>PHYTEC</ins> et n'embarque que le strict minimum.
4. Le **DTB** permet de manipuler le noyau linux en effectuant des modifications sans avoir besoin de le recompiler.
5. Le **rootFS** est l'ensemble des fichiers nécessaires (système de fichiers, arborescence, utilisateurs, etc ...).
___

Problèmes liés a la VM
----------------------

!!! bug
    Lors de la connection en **RS232**, le **pointeur de souris** peut être complètement **buguer** (l'icône ne suit pas la position réelle du curseur de souris).

___

00 - Carte SD
-------------

Réalisation d'une **carte SD** avec le contenu nécessaire au flashage d'un module <ins>PHYTEC</ins>.

Disposer d'une **carte SD** d'<ins>au moins 1 Go</ins> vierge ainsi qu'un adaptateur ou un lecteur.

1. Connecter la **carte SD** sous windows à l'aide de l'adaptateur ou du lecteur.
1. Dans l'explorateur de fichiers windows, afficher les périphériques connectés et faire un <ins>clique droit</ins> sur la **carte SD**.
1. Sélectionner `Formater...`.

![Windows carte SD formatage menu](img/windows_carte_sd_formatage_menu.png)

1. `Dans la boîte de dialogue`, Système de fichiers, <ins>sélectionner FAT32 (par défaut)</ins>.
1. `Dans la boîte de dialogue`, Options de formatage, <ins>décocher Formatage rapide</ins>.
1. Appuyer sur <ins>Démarrer</ins>.

![Windows carte SD formatage](img/windows_carte_sd_formatage.png)

Une fois le <ins>formatage terminé</ins>.

Copier le contenu du dossier `P:\Electronique\Distributeur\LIVRABLES_DISTRIBUTEUR_(SOFT)\3_Systeme\IMAGES_DISTRIBUTEUR_PD15` sur la **carte SD**.

Une fois la <ins>copie terminée</ins>.

1. Éjecter la **carte SD** de Windows.
2. Insérer la **carte SD** dans le slot 1 de la <ins>carte Optiweb</ins>.
___

01 - Carte Centrale Optiweb
---------------------------

S'assurer que la **carte SD** est présente dans le slot <ins>SD 1</ins> de la carte Optiweb.

??? info "slot SD1 (2Go maxi)"
    Lors de la réalisation de la carte Optiweb, le BSP ne supportait pas les cartes d'une capacité supérieure à 2Go, aujourd'hui ce problème est corrigé.

![Carte Optiweb](img/carte_optiweb.jpg)

1. Connecter la <ins>liaison série</ins> au port RS-232 étiqueté **CONSOLE**.
1. Connecter le <ins>cable ethernet</ins> au port RJ-45 etiqueté **ETHERNET**.
1. Connecter l'<ins>alimentation</ins> au port étiqueté **ALIM 230V**.

!!! danger
    Ne <ins>SURTOUT PAS CONNECTER</ins> le <ins>cable d'alimentation</ins> au port étiqueté **ALIM ÉCRAN** sous risque de faire exploser des composants de la carte Optiweb!

![Carte Optiweb connectee](img/carte_optiweb_connectee.jpg)

___

02 - Liaison RS-232
-------------------

Pour établir la communication entre la vm et le module <ins>PHYTEC</ins>, il faut créer la connexion à l'aide d'un **adaptateur RS-232**,
branché en **USB à l'ordinateur** éxécutant la VM et le **port série** à la carte de la centrale sur le **port étiqueté CONSOLE** qui correspond à l'**UART 0**.
___

03 - Mise sous tension
----------------------

Une fois toutes les <ins>étapes précédentes réalisées</ins>, mettre la **centrale Optiweb sous tension**.

!!! danger
    Ne pas hésiter à **vérifier que le cable d'alimentation** en 230V est bien dans le <ins>port sous le relais</ins> et non à droite de la carte!
___

04 - Rafraichir les USB
-----------------------

!!! attention
    Il est préférable de **quitter tous les programmes** avant d'établir la liaison en **RS232** qui peut faire **buguer** la vm.

Barre de menu de virtual box > **Périphériques** > **USB** > **Paramêtres USB ...**

![VirtualBox USB parametres](img/virtualbox_parametres_usb.png)

Faire simplement **apparaitre la fenêtre** pour rafraichir les USB puis cliquer sur **Annuler**.

![VirtualBox USB activate controller](img/virtualbox_activate_usb_controller.png)
___

05 - Sélectionner FDTI USB
--------------------------

Lorsque que le controlleur USB est activé, selectionné **FDTI USB**, pour établir la connexion en **RS232**.

![VirtualBox USB activer connectés](img/virtualbox_usb_activer_connectes.png)
___

06 - Terminal BareBox
---------------------

Ouvrir le terminal de **Barebox** à l'aide du lien **Microcom_ttyUSB0** ou **Microcom_ttyUSB1**.

![Ubuntu desktop microcom ttyusb](img/ubuntu_desktop_microcom_ttyusb.png)

Appuyer sur la **touche entrée** pour faire apparaitre le <ins>prompt barebox</ins>.

![Microcom ttyusb barebox prompt](img/microcom_ttyusb_barebox_prompt.png)
