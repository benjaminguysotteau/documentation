À Propos
========

Installation
------------

```sh
sudo pip install \
  mkdocs \
  pygments \
  mkdocs-material \
  mkdocs-extensions \
  pymdown-extensions
```


Utilisation
-----------

```sh
cd ${HOME}/b2c/doc
mkdocs serve
firefox http://localhost:8000
```
