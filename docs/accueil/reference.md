URL (Liens cliquables)
======================

+ [Optiweb](https://www.optiweb-access.fr/login)
+ [Optiweb Beta](https://beta-heurtaux.siqual.net/login)
+ [Optiweb Beta Admin](https://beta-heurtaux.siqual.net/admin/utilisateurs/)

+ [Kelio](http://srv-prod:8089/open/login)
___

+ [Markdown cheatSheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
+ [Markdown Github Help](https://help.github.com/articles/basic-writing-and-formatting-syntax/)
___

+ [Mkdocs Documentation](https://www.mkdocs.org/)
___

+ [Mkdocs Material Admonition](https://squidfunk.github.io/mkdocs-material/extensions/admonition/)
+ [Mkdocs Material Caret](https://facelessuser.github.io/pymdown-extensions/extensions/caret/)
+ [Mkdocs Material Critic](https://facelessuser.github.io/pymdown-extensions/extensions/critic/)
+ [Mkdocs Material Details](https://facelessuser.github.io/pymdown-extensions/extensions/details/)
+ [Mkdocs Material Tasklist](https://facelessuser.github.io/pymdown-extensions/extensions/tasklist/)
