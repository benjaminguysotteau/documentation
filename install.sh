#!/bin/bash
################################################################################
#
# Filename: install.sh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-06-25
#
################################################################################
#set -x

source ./libsh/print.sh
source ./libsh/install.sh


# package=(python [...])
# OR
declare -a package=(python \
                    python3 \
                    pip \
                    python-testresources \
                    mkdocs)
package_length=${#package[@]}
package_length_minus_1=$((package_length - 1))

declare -a package_to_install=()

# On parcourt le tableau des paquets nécessaires au projet,
# pour chacun des paquets on vérifie s'il est installé ou non.
# Si un paquet n'est pas installé et qu'il est différent de pip,
# on l'ajoute au tableau package_to_install.
# Sinon on passe le flag pip_package_must_be_installed.
for i in $(seq 0 $package_length_minus_1)
do
  if [[ $(is_install ${package[i]}) -eq 1 ]]; then
    print_formatted ok "${package[i]} package is installed."
    
  else
    print_formatted warn "${package[i]} package isn't installed!"
    
    if [[ ! ${package[i]} = "pip" ]]; then
      package_to_install+=(${package[i]})
    else
      pip_package_must_be_installed="true"
    fi

  fi
done

if [[ ${#package_to_install[@]} -eq 0 ]]; then
  print_formatted info "All required packages are installed."
  exit 1
fi

install_recommends ${package_to_install[@]}

if [[ $pip_package_must_be_installed = "true" ]]; then
  print_formatted info "Sudo password required to install pip package:"
  sudo python3 get-pip.py
fi

#set +x
exit 0
